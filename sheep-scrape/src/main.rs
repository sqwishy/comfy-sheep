#[macro_use]
extern crate serde_derive;

extern crate clap;

extern crate chrono;

extern crate reqwest;

extern crate itertools;

extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_journald;
extern crate slog_term;

extern crate postgres;

extern crate comfy;

use std::fmt::Debug;
use std::sync::mpsc;
use std::thread;
use std::time;

use clap::Arg;

use reqwest::{StatusCode, Url};

mod db;
mod helix;
mod kraken;
mod scrape;
mod twitch;

fn main() {
    let matches = clap::App::new("sheep-scrape")
        .author("somebody@froghat.ca")
        .version("0.0.0")
        .arg(Arg::from_usage("--journal 'log to the journal'"))
        .arg(Arg::from_usage("--db [val] 'pg connection string'").required(true))
        .arg(
            Arg::from_usage("--twitch-client-id [val] 'client id used to talk to twitch'")
                .required(true),
        )
        .arg(Arg::from_usage(
            "--twitch-oauth [val] 'OAuth token used to talk to twitch'",
        ))
        .get_matches();

    let log = comfy::new_logger(&matches);
    debug!(log, "Hello, world! {:?}", matches);

    let connstr = matches.value_of("db").unwrap();
    let twitch_client_id = matches.value_of("twitch-client-id").unwrap();
    let twitch_oauth_token = matches.value_of("twitch-oauth").map(|s| s.to_owned());
    let client = Client::new(twitch_client_id.to_owned(), twitch_oauth_token, log.clone());

    scrape::pull_kraken_streams(&log, &client, connstr).unwrap();

    // get chatters...
    //let res = client.get_chatters("destiny").unwrap();
    //println!("{:#?}", res);

    // get users
    //let res = client
    //    .get_users(helix::GetUsers {
    //        logins: vec!["mrmouton", "destiny", "tvrna"],
    //        ..Default::default()
    //    })
    //    .unwrap();
    //println!("{:#?}", res);

    // pages?...
    //let (tx, rx) = mpsc::channel();
    //thread::spawn(move || {
    //    client
    //        .send_page_items::<helix::HelixPage<helix::Stream>>(tx)
    //        .unwrap();
    //});
    //for items in rx.iter() {
    //    println!("{:#?}", items);
    //}
}

#[derive(Debug)]
pub enum Error {
    Io(std::io::Error),
    Http(reqwest::Error),
    RateLimited(time::Duration),
    Json(String, serde_json::Error),
    Backoff(BackoffError),
    Whatever(String),
}

impl From<BackoffError> for Error {
    fn from(err: BackoffError) -> Error {
        Error::Backoff(err)
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Error {
        Error::Http(err)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::Io(err)
    }
}

// type Result<T> = Result<T, Error>;

/// This is based on helix pagination where it gives you a cursor ... it should probably be
/// refactored to mean that specifically. (Is calling this a page even correct?)
pub trait CursorPage {
    type Item: Debug;
    fn first() -> Url;
    fn into_items(self) -> Vec<Self::Item>;
    fn next(&self) -> Option<Url>;
}

/// A resource that can produce Pages.
/// This is for offset & limit pagination like with kraken...
pub trait Paginated {
    type Page: Page + Debug + for<'de> serde::Deserialize<'de>;

    const MAX_LIMIT: u32;

    /// The web address to a page?
    fn page_url(limit: u32, offset: u32) -> Url;

    //fn get_page(limit: usize, offset: usize) -> Result<Self::Page, Error>;
}

pub trait Page {
    type Item: Debug;

    fn total_hint(&self) -> Option<u32>;
    // shouldn't these use iterator traits or something?
    fn items(&self) -> &Vec<Self::Item>;
    fn into_items(self) -> Vec<Self::Item>;
}

pub struct Client {
    log: slog::Logger,
    client: reqwest::Client,
    client_id: String,
    oauth_token: Option<String>,
}

impl Client {
    pub fn new(client_id: String, oauth_token: Option<String>, log: slog::Logger) -> Self {
        let client = reqwest::Client::new();
        Client {
            log,
            client,
            client_id,
            oauth_token,
        }
    }

    pub fn get_users(&self, g: helix::GetUsers) -> Result<Vec<helix::User>, Error> {
        let resp: helix::Data<helix::User> = self.get(g.url())?;
        Ok(resp.data)
    }

    pub fn send_page_items<P>(&self, tx: mpsc::Sender<Vec<P::Item>>) -> Result<(), Error>
    where
        P: for<'de> serde::Deserialize<'de> + CursorPage + Debug + 'static,
    {
        let mut url = P::first();
        loop {
            let page: P = match self.get(url.clone()) {
                Ok(v) => v,
                Err(Error::RateLimited(duration)) => {
                    eprintln!("rate limited ... waiting {:?}", duration);
                    thread::sleep(duration);
                    continue;
                }
                Err(e) => return Err(e),
            };

            url = if let Some(url) = page.next() {
                url
            } else {
                break; // TODO this breaks too soon before we get the page items ...
            };

            let items = page.into_items();
            if items.is_empty() {
                break;
            } else {
                tx.send(items).map_err(|e| Error::Whatever(e.to_string()))?;
            }
        }
        Ok(())
    }

    pub fn prepare_headers(&self, builder: reqwest::RequestBuilder) -> reqwest::RequestBuilder {
        let builder = builder
            .header("Accept", "application/vnd.twitchtv.v5+json") // TODO this is stupid, this is kraken specific
            .header("Client-Id", self.client_id.clone());
        if let Some(token) = &self.oauth_token {
            builder.bearer_auth(token)
        } else {
            builder
        }
    }

    pub fn get<T>(&self, url: reqwest::Url) -> Result<T, Error>
    where
        T: for<'de> serde::Deserialize<'de> + 'static,
    {
        let builder = self.client.get(url);
        let builder = self.prepare_headers(builder);
        let req = builder.build()?;
        // debug!(self.log, "sending {:?}", req);
        let res = self.client.execute(req)?;
        let status = res.status();
        if status == StatusCode::TOO_MANY_REQUESTS {
            let backoff = helix::maybe_backoff(&res)?;
            Err(Error::RateLimited(backoff))
        } else {
            let mut res = res.error_for_status()?;
            let mut buf = Vec::new();
            use std::io::Read;
            res.read_to_end(&mut buf)?;
            Ok(serde_json::from_slice(&buf).map_err(|e| {
                Error::Json(
                    match String::from_utf8(buf) {
                        Ok(text) => text,
                        Err(e) => format!("not utf-8 lol: {}\n{:#?}", e, e.as_bytes()),
                    },
                    e,
                )
            })?)
        }
    }
}

#[derive(Debug)]
pub enum BackoffError {
    MissingHeader(String),
    Utf8(std::str::Utf8Error),
    Int(std::num::ParseIntError),
    Chrono(chrono::ParseError),
}

impl From<std::str::Utf8Error> for BackoffError {
    fn from(err: std::str::Utf8Error) -> BackoffError {
        BackoffError::Utf8(err)
    }
}

impl From<std::num::ParseIntError> for BackoffError {
    fn from(err: std::num::ParseIntError) -> BackoffError {
        BackoffError::Int(err)
    }
}

impl From<chrono::ParseError> for BackoffError {
    fn from(err: chrono::ParseError) -> BackoffError {
        BackoffError::Chrono(err)
    }
}
