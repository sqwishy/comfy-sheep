use super::{Page, Paginated, Url};

#[derive(Deserialize, Debug)]
pub struct Channel {
    #[serde(rename = "_id")]
    pub id: i64,
    #[serde(default)]
    pub broadcaster_language: String,
    #[serde(default)]
    pub broadcaster_software: String,
    #[serde(default)]
    pub broadcaster_type: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    #[serde(default)]
    pub description: String,
    pub display_name: String,
    pub followers: i64,
    // pub game: String,
    #[serde(default)]
    pub language: String,
    #[serde(default)]
    pub logo: Option<String>,
    #[serde(rename = "mature")]
    pub is_mature: bool,
    pub name: String,
    #[serde(rename = "partner")]
    pub is_partner: bool,
    //pub privacy_options_enabled: bool,
    //#[serde(rename = "private_video")]
    //pub is_private: bool,
    #[serde(default)]
    pub profile_banner: Option<String>, // Actually a URL
    #[serde(default)]
    pub profile_banner_background_color: String,
    #[serde(default)]
    pub status: String,
    pub updated_at: chrono::DateTime<chrono::Utc>,
    pub url: String, // Actually a URL
    #[serde(default)]
    pub video_banner: Option<String>, // Actually a URL
    pub views: i64,
}

#[derive(Deserialize, Debug)]
pub struct Stream {
    #[serde(rename = "_id")]
    pub id: i64,
    pub average_fps: Option<f32>,
    pub channel: Channel,
    #[serde(default)]
    pub broadcast_platform: String,
    //#[serde(default)]
    //pub community_id: String,
    //#[serde(default)]
    //pub community_ids: Vec<String>,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub delay: f32,
    pub game: String,
    pub is_playlist: bool,
    //"preview": {
    //    "large": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-640x360.jpg",
    //    "medium": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-320x180.jpg",
    //    "small": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-80x45.jpg",
    //    "template": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-{width}x{height}.jpg"
    //},
    //pub stream_type: live,
    pub video_height: i32,
    pub viewers: i64,
}

#[derive(Deserialize, Debug)]
pub struct StreamPage {
    pub _total: Option<i32>,
    pub streams: Vec<Stream>,
}

impl Paginated for StreamPage {
    type Page = Self;

    const MAX_LIMIT: u32 = 100;

    fn page_url(offset: u32, limit: u32) -> Url {
        let limit = format!("{}", limit);
        let offset = format!("{}", offset);
        Url::parse_with_params(
            "https://api.twitch.tv/kraken/streams",
            &[("limit", limit), ("offset", offset)],
        )
        .unwrap()
    }
}

impl Page for StreamPage {
    type Item = Stream;

    fn total_hint(&self) -> Option<u32> {
        self._total.map(|total| total as u32)
    }

    fn items(&self) -> &Vec<Self::Item> {
        &self.streams
    }

    fn into_items(self) -> Vec<Self::Item> {
        self.streams
    }
}

#[test]
fn stream() {
    let data = r#"
    {
        "_total": 27672,
        "streams": [
            {
                "_id": 32710496752,
                "average_fps": 59.9400599401,
                "broadcast_platform": "live",
                "channel": {
                    "_id": 124425501,
                    "broadcaster_language": "en",
                    "broadcaster_software": "",
                    "broadcaster_type": "",
                    "created_at": "2016-05-16T18:35:22.08304Z",
                    "description": "For more information, schedules and stats head to lolesports.com",
                    "display_name": "lck",
                    "followers": 508963,
                    "game": "League of Legends",
                    "language": "en",
                    "logo": "https://static-cdn.jtvnw.net/jtv_user_pictures/9c06645c-0ba0-4f53-8a41-8208a0e36043-profile_image-300x300.png",
                    "mature": false,
                    "name": "lck",
                    "partner": true,
                    "privacy_options_enabled": false,
                    "private_video": false,
                    "profile_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/lck-profile_banner-bf4e5c36656b7d28-480.png",
                    "profile_banner_background_color": "",
                    "status": "HLE vs. GEN - SKT vs. KZ | Week 4 Day 1 | LCK Spring Split (2019)",
                    "updated_at": "2019-02-13T10:31:47.179854Z",
                    "url": "https://www.twitch.tv/lck",
                    "video_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/a9c2270f081e6ad3-channel_offline_image-1920x1080.jpeg",
                    "views": 49412976
                },
                "community_id": "",
                "community_ids": [],
                "created_at": "2019-02-13T07:31:40Z",
                "delay": 0,
                "game": "League of Legends",
                "is_playlist": false,
                "preview": {
                    "large": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-640x360.jpg",
                    "medium": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-320x180.jpg",
                    "small": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-80x45.jpg",
                    "template": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-{width}x{height}.jpg"
                },
                "stream_type": "live",
                "video_height": 1080,
                "viewers": 15072
            },
            {
                "_id": 32708596864,
                "average_fps": 60,
                "broadcast_platform": "live",
                "channel": {
                    "_id": 152596920,
                    "broadcaster_language": "ko",
                    "broadcaster_software": "",
                    "broadcaster_type": "",
                    "created_at": "2017-04-07T04:07:21.364531Z",
                    "description": "",
                    "display_name": "앰비션_",
                    "followers": 122572,
                    "game": "League of Legends",
                    "language": "ko",
                    "logo": "https://static-cdn.jtvnw.net/jtv_user_pictures/8d9d49f3-d744-495b-ba28-facc57dc9381-profile_image-300x300.png",
                    "mature": false,
                    "name": "lol_ambition",
                    "partner": true,
                    "privacy_options_enabled": false,
                    "private_video": false,
                    "profile_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/31aaf5b3-439f-4fcc-bc34-d03bb916e68a-profile_banner-480.png",
                    "profile_banner_background_color": "",
                    "status": "앰비션/ 솔랭한판하고 롤챔스 보기!",
                    "updated_at": "2019-02-13T10:31:05.025511Z",
                    "url": "https://www.twitch.tv/lol_ambition",
                    "video_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/ef9f0a61-773c-4161-aec8-2bd6968c2d36-channel_offline_image-1920x1080.png",
                    "views": 4382333
                },
                "community_id": "",
                "community_ids": [],
                "created_at": "2019-02-13T03:57:09Z",
                "delay": 0,
                "game": "League of Legends",
                "is_playlist": false,
                "preview": {
                    "large": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lol_ambition-640x360.jpg",
                    "medium": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lol_ambition-320x180.jpg",
                    "small": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lol_ambition-80x45.jpg",
                    "template": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lol_ambition-{width}x{height}.jpg"
                },
                "stream_type": "live",
                "video_height": 1080,
                "viewers": 13541
            }
        ]
    }
    "#;
    let _strims: StreamPage = serde_json::from_str(data).unwrap();
}

#[test]
fn channel() {
    let data = r#"
    {
      "mature": true,
      "partner": false,
      "status": " !Merch | Come Play !join  | !code !soundcommands",
      "broadcaster_language": "en",
      "broadcaster_software": "",
      "display_name": "DaPotatoKing_",
      "game": "Fortnite",
      "language": "en",
      "_id": 223073807,
      "name": "dapotatoking_",
      "created_at": "2018-05-14T21:58:30Z",
      "updated_at": "2019-02-26T19:40:48Z",
      "delay": null,
      "logo": null,
      "banner": null,
      "video_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/7073b7b2-3e9d-46d1-ba71-60315eda377d-channel_offline_image-1920x1080.png",
      "background": null,
      "profile_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/6ffceecd-2ec9-44e7-8988-9f55d6df1763-profile_banner-480.png",
      "profile_banner_background_color": "",
      "url": "https://www.twitch.tv/dapotatoking_",
      "views": 35635,
      "followers": 2228,
      "_links": {
        "self": "https://api.twitch.tv/kraken/channels/dapotatoking_",
        "follows": "https://api.twitch.tv/kraken/channels/dapotatoking_/follows",
        "commercial": "https://api.twitch.tv/kraken/channels/dapotatoking_/commercial",
        "stream_key": "https://api.twitch.tv/kraken/channels/dapotatoking_/stream_key",
        "chat": "https://api.twitch.tv/kraken/chat/dapotatoking_",
        "features": "https://api.twitch.tv/kraken/channels/dapotatoking_/features",
        "subscriptions": "https://api.twitch.tv/kraken/channels/dapotatoking_/subscriptions",
        "editors": "https://api.twitch.tv/kraken/channels/dapotatoking_/editors",
        "teams": "https://api.twitch.tv/kraken/channels/dapotatoking_/teams",
        "videos": "https://api.twitch.tv/kraken/channels/dapotatoking_/videos"
      }
    }"#;
    let _channel: Channel = serde_json::from_str(data).unwrap();
}
