use std::fmt::Debug;
use std::time;

use super::{BackoffError, CursorPage, Url};

const HELIX: &str = "https://api.twitch.tv/helix";
//match Url::builder()
//    .scheme("https")
//    .authority("api.twitch.tv")
//    .path_and_query("/helix")
//    .build()?

// https://dev.twitch.tv/docs/api/reference/#get-streams
// enum StreamType {
//     Live,
//     Unknown,
// }

#[derive(Deserialize, Debug)]
pub struct Stream {
    pub community_ids: Option<Vec<String>>, // Array of community IDs.
    pub game_id: String,                    // ID of the game being played on the stream.
    pub id: String,                         // Stream ID.
    pub language: String,                   // Stream language.
    pub started_at: String,                 // UTC timestamp.
    pub tag_ids: Option<Vec<String>>,       // Shows tag IDs that apply to the stream.
    pub thumbnail_url: String, // Thumbnail URL of the stream. All image URLs have variable width and height. You can replace {width} and {height} with any values to get that size image
    pub title: String,         // Stream title.
    #[serde(rename = "type")]
    pub tipe: String, // Stream type: "live" or "" (in case of error).
    pub user_id: String,       // ID of the user who is streaming.
    pub user_name: String,     // Login name corresponding to user_id.
    pub viewer_count: i32,     // Number of viewers watching the stream at the time of the query.
}

#[derive(Deserialize, Debug)]
pub struct User {
    pub broadcaster_type: String, // broadcaster type: "partner", "affiliate", or "".
    pub description: String,      // channel description.
    pub display_name: String,     // display name.
    pub email: Option<String>, // email address. Returned if the request includes the user:read:email scope.
    pub id: String,            // ID.
    pub login: String,         // login name.
    pub offline_image_url: Option<String>, // URL of the user's offline image.
    pub profile_image_url: Option<String>, // URL of the user's profile image.
    #[serde(rename = "type")]
    pub tipe: String, // type: "staff", "admin", "global_mod", or "".
    pub view_count: i32,       // Total number of views of the user’s channel.
}

pub fn maybe_backoff(res: &reqwest::Response) -> Result<time::Duration, BackoffError> {
    let headers = res.headers();
    eprintln!("your ratelimit-limit: {:?}", headers.get("ratelimit-limit"));

    let date_string = headers
        .get("date")
        .ok_or(BackoffError::MissingHeader("date".to_string()))
        .and_then(|v| Ok(std::str::from_utf8(v.as_bytes())?))?;
    let date = chrono::DateTime::parse_from_rfc2822(date_string)?.naive_utc();

    let reset_from_epoch: i64 = headers
        .get("ratelimit-reset")
        .ok_or(BackoffError::MissingHeader("ratelimit-reset".to_string()))
        .and_then(|v| Ok(std::str::from_utf8(v.as_bytes())?))?
        .parse()?;
    let reset = chrono::NaiveDateTime::from_timestamp(reset_from_epoch, 0);

    // Any overflow/underflow errors are ignored...
    Ok((reset - date).to_std().unwrap_or(time::Duration::new(0, 0)))
}

impl<T: Debug> CursorPage for HelixPage<T> {
    type Item = T;

    fn first() -> Url {
        format!("{}/streams?first=100", HELIX).parse().unwrap()
    }

    fn into_items(self) -> Vec<T> {
        self.data
    }

    fn next(&self) -> Option<Url> {
        match &self.pagination.cursor {
            Some(cursor) => Some(
                format!("{}&after={}", Self::first(), cursor)
                    .parse()
                    .unwrap(),
            ),
            None => None,
        }
    }
}

// TODO define an alias type or something maybe to clearly define where values are expected to be
// sanitized...
#[derive(Default)]
pub struct GetUsers<'l> {
    pub ids: Vec<&'l str>,
    pub logins: Vec<&'l str>,
}

impl<'l> GetUsers<'l> {
    pub fn url(&self) -> Url {
        let root = format!("{}/users", HELIX);
        let id_pairs = self.ids.iter().map(|v| ("id", v));
        let login_pairs = self.logins.iter().map(|v| ("login", v));
        let pairs = id_pairs.chain(login_pairs);
        Url::parse_with_params(&root, pairs).unwrap()
    }
}

//impl<'l> reqwest::IntoUrl for GetUsers<'l> {
//    fn into_url(self) -> reqwest::Result<Url> {
//        Ok(format!("{}/users", HELIX).parse().unwrap())
//    }
//}

//impl Request<'static> for GetUsers {
//    type Response = HelixPage<User>;
//}

#[derive(Deserialize, Debug)]
pub struct Data<T> {
    pub data: Vec<T>,
}

#[derive(Deserialize, Debug)]
pub struct Pagination {
    pub cursor: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct HelixPage<T> {
    pub data: Vec<T>,
    pub pagination: Pagination,
}

#[test]
fn helix_streams_page() {
    let data = r#"
    {
      "data": [
        {
          "id": "11111111111",
          "user_id": "11111111",
          "user_name": "xxxxxxxxx",
          "game_id": "11111",
          "community_ids": [
            "00000000-0000-0000-0000-000000000000"
          ],
          "type": "live",
          "title": "xxxxxxxxxxxxxxxxxxxx",
          "viewer_count": 11111,
          "started_at": "2019-01-01T01:01:01Z",
          "language": "en",
          "thumbnail_url": "https://static-cdn.jtvnw.net/previews-ttv/live_user_anonymous-{width}x{height}.jpg",
          "tag_ids": [
            "00000000-0000-0000-0000-000000000000"
          ]
        },
        {
          "id": "11111111111",
          "user_id": "11111111",
          "user_name": "xxxxxxxxx",
          "game_id": "11111",
          "community_ids": null,
          "type": "live",
          "title": "xxxxxxxxxxxxxxxxxxxx",
          "viewer_count": 11111,
          "started_at": "2019-01-01T01:01:01Z",
          "language": "en",
          "thumbnail_url": "https://static-cdn.jtvnw.net/previews-ttv/live_user_anonymous-{width}x{height}.jpg",
          "tag_ids": null
        }
      ],
      "pagination": {
        "cursor": "420 blaze it"
      }
    }
    "#;
    let _strims: HelixPage<Stream> = serde_json::from_str(data).unwrap();
}

#[test]
fn helix_user() {
    let data = r#"
    {
        "data": [
            {
                "broadcaster_type": "",
                "description": "",
                "display_name": "flusenwaffen",
                "id": "266355205",
                "login": "flusenwaffen",
                "offline_image_url": "",
                "profile_image_url": "https://static-cdn.jtvnw.net/user-default-pictures/cd618d3e-f14d-4960-b7cf-094231b04735-profile_image-300x300.jpg",
                "type": "",
                "view_count": 1
            }
        ]
    }
    "#;
    let _u: Data<User> = serde_json::from_str(data).unwrap();
}
