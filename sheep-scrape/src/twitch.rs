use super::Url;

#[derive(Deserialize, Debug)]
pub struct Chatters {
    pub admins: Option<Vec<String>>,
    pub broadcaster: Option<Vec<String>>,
    pub global_mods: Option<Vec<String>>,
    pub moderators: Option<Vec<String>>,
    pub staff: Option<Vec<String>>,
    pub viewers: Option<Vec<String>>,
    pub vips: Option<Vec<String>>,
}

#[derive(Deserialize, Debug)]
pub struct ChattersResponse {
    //_links
    pub chatter_count: i64,
    pub chatters: Chatters,
}

impl ChattersResponse {
    pub fn endpoint(channel: &str) -> Url {
        format!("https://tmi.twitch.tv/group/user/{}/chatters", channel)
            .parse()
            .unwrap()
    }
}

#[test]
fn stream() {
    let data = r#"
    {
        "_total": 27672,
        "streams": [
            {
                "_id": 32710496752,
                "average_fps": 59.9400599401,
                "broadcast_platform": "live",
                "channel": {
                    "_id": 124425501,
                    "broadcaster_language": "en",
                    "broadcaster_software": "",
                    "broadcaster_type": "",
                    "created_at": "2016-05-16T18:35:22.08304Z",
                    "description": "For more information, schedules and stats head to lolesports.com",
                    "display_name": "lck",
                    "followers": 508963,
                    "game": "League of Legends",
                    "language": "en",
                    "logo": "https://static-cdn.jtvnw.net/jtv_user_pictures/9c06645c-0ba0-4f53-8a41-8208a0e36043-profile_image-300x300.png",
                    "mature": false,
                    "name": "lck",
                    "partner": true,
                    "privacy_options_enabled": false,
                    "private_video": false,
                    "profile_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/lck-profile_banner-bf4e5c36656b7d28-480.png",
                    "profile_banner_background_color": "",
                    "status": "HLE vs. GEN - SKT vs. KZ | Week 4 Day 1 | LCK Spring Split (2019)",
                    "updated_at": "2019-02-13T10:31:47.179854Z",
                    "url": "https://www.twitch.tv/lck",
                    "video_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/a9c2270f081e6ad3-channel_offline_image-1920x1080.jpeg",
                    "views": 49412976
                },
                "community_id": "",
                "community_ids": [],
                "created_at": "2019-02-13T07:31:40Z",
                "delay": 0,
                "game": "League of Legends",
                "is_playlist": false,
                "preview": {
                    "large": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-640x360.jpg",
                    "medium": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-320x180.jpg",
                    "small": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-80x45.jpg",
                    "template": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lck-{width}x{height}.jpg"
                },
                "stream_type": "live",
                "video_height": 1080,
                "viewers": 15072
            },
            {
                "_id": 32708596864,
                "average_fps": 60,
                "broadcast_platform": "live",
                "channel": {
                    "_id": 152596920,
                    "broadcaster_language": "ko",
                    "broadcaster_software": "",
                    "broadcaster_type": "",
                    "created_at": "2017-04-07T04:07:21.364531Z",
                    "description": "",
                    "display_name": "앰비션_",
                    "followers": 122572,
                    "game": "League of Legends",
                    "language": "ko",
                    "logo": "https://static-cdn.jtvnw.net/jtv_user_pictures/8d9d49f3-d744-495b-ba28-facc57dc9381-profile_image-300x300.png",
                    "mature": false,
                    "name": "lol_ambition",
                    "partner": true,
                    "privacy_options_enabled": false,
                    "private_video": false,
                    "profile_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/31aaf5b3-439f-4fcc-bc34-d03bb916e68a-profile_banner-480.png",
                    "profile_banner_background_color": "",
                    "status": "앰비션/ 솔랭한판하고 롤챔스 보기!",
                    "updated_at": "2019-02-13T10:31:05.025511Z",
                    "url": "https://www.twitch.tv/lol_ambition",
                    "video_banner": "https://static-cdn.jtvnw.net/jtv_user_pictures/ef9f0a61-773c-4161-aec8-2bd6968c2d36-channel_offline_image-1920x1080.png",
                    "views": 4382333
                },
                "community_id": "",
                "community_ids": [],
                "created_at": "2019-02-13T03:57:09Z",
                "delay": 0,
                "game": "League of Legends",
                "is_playlist": false,
                "preview": {
                    "large": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lol_ambition-640x360.jpg",
                    "medium": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lol_ambition-320x180.jpg",
                    "small": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lol_ambition-80x45.jpg",
                    "template": "https://static-cdn.jtvnw.net/previews-ttv/live_user_lol_ambition-{width}x{height}.jpg"
                },
                "stream_type": "live",
                "video_height": 1080,
                "viewers": 13541
            }
        ]
    }
    "#;
}
