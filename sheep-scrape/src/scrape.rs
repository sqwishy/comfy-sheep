use itertools::Itertools;
use std::sync::{Arc, Mutex};
use std::thread::sleep;
use std::time::Duration;

use crate::db;
use crate::kraken;
use crate::{Client, Error, Url};
use crate::{Page, Paginated};

use crossbeam::channel::{self, Sender};
use crossbeam::thread;

type RequesterResult = Result<(kraken::StreamPage, chrono::DateTime<chrono::Utc>), Error>;

const WORKER_COUNT: u32 = 32;

/// This will panic if any interior threads panic...
pub fn pull_kraken_streams(
    log: &slog::Logger,
    client: &Client,
    connstr: &str,
) -> Result<(), Error> {
    let pg = postgres::Connection::connect(connstr, postgres::TlsMode::None)
        .map_err(|e| Error::Whatever(e.to_string()))?;

    thread::scope(|s| {
        let pg = pg;

        let limit = kraken::StreamPage::MAX_LIMIT;
        // if we trying to query a million items we've probably made a mistake?
        let sanity = 1_000_000;
        let overlap = ((limit as f32) * 0.75) as u32;
        let offset_limits = (0..).map(move |n| (n * (limit - overlap), limit));
        let urls = offset_limits
            .take_while(move |(offset, _)| offset < &sanity)
            .map(|(offset, limit)| kraken::StreamPage::page_url(offset, limit));
        // I think I should be able to do this without an Arc but I don't know how...
        let urls = Arc::new(Mutex::new(urls));

        let (res_rx, worker_handles) = {
            let (res_tx, res_rx) = channel::unbounded();

            let worker_handles = (0..WORKER_COUNT)
                .map(|_| {
                    let res_tx = res_tx.clone();
                    let urls = urls.clone();
                    s.spawn(|_| {
                        let urls = urls;
                        let res_tx = res_tx;
                        requester_loop(log, client, urls, res_tx);
                    })
                })
                .collect::<Vec<_>>();

            (res_rx, worker_handles)
        };

        let chunked = res_rx
            .into_iter()
            .filter_map(|res| match res {
                // Be loud about deserialization errors, but don't consider them fatal
                Err(Error::Json(json, err)) => {
                    let hint = json_error_hint(&json, &err).map(|s| s.to_owned());;
                    error!(
                        log,
                        "json parsing error (fix your schema): {}", err;
                        "json" => json,
                        "hint" => hint,
                    );
                    None
                }
                Err(err) => {
                    error!(log, "request error! {:#?}", err);
                    None
                }
                Ok((page, time)) => {
                    page.into_items()
                        .into_iter()
                        .map(move |inner| db::Sampled { time, inner })
                }
                .into(),
            })
            .flatten()
            .unique_by(|sampled| sampled.inner.id)
            .chunks(8 * 1000);

        for samples in &chunked {
            let watch = comfy::Stopwatch::start();
            let all: Vec<_> = samples.collect(); // this blocks while we're waiting for results
            let (watch, wait_time) = watch.reset();

            let mut attempts = (1..30).map(|x| Duration::from_millis(x * 500));
            let (num_new, num_dup) = loop {
                match save_some_shit(&mut all.iter(), &pg) {
                    Ok(ok) => break ok,
                    Err(e) => {
                        error!(log, "Failed to commit streams. {}", e;
                               "error" => format!("{}", e));
                        if let Some(delay) = attempts.next() {
                            sleep(delay);
                            continue;
                        } else {
                            panic!("failed after several attempts: {}", e)
                        }
                    }
                }
            };
            info!(log, "Committed streams.";
                  "inserted" => num_new,
                  "duplicates" => num_dup,
                  "delay" => comfy::Logable::from(wait_time),
                  "duration" => comfy::Logable::from(watch.since()));
        }

        let res = worker_handles
            .into_iter()
            .map(|h| h.join())
            .collect::<Vec<_>>();

        info!(log, "wow! {:?}", res);
    })
    .unwrap();
    Ok(())
}

pub fn json_error_hint<'a>(json: &'a str, err: &'a serde_json::Error) -> Option<&'a str> {
    if err.line() == 1 {
        use std::cmp::{max, min};
        let col = err.column();
        Some(&json[max(0, col - 20)..min(json.len(), col + 10)])
    } else {
        None
    }
}

fn requester_loop<I>(
    log: &slog::Logger,
    client: &Client,
    urls: Arc<Mutex<I>>,
    res_tx: Sender<RequesterResult>,
) where
    I: Iterator<Item = Url>,
{
    let mut error_tolerance: usize = 4;
    loop {
        let url = {
            let mut it = urls.lock().expect("failed to lock url iterator");
            match it.next() {
                Some(url) => url,
                None => {
                    error!(log, "Ran out of URLs to scrape; this is a sanity check and we probably went insane");
                    break;
                }
            }
        };
        let mut retries = 0..5;
        let res = loop {
            let watch = comfy::Stopwatch::start();
            let res: Result<kraken::StreamPage, Error> = client.get(url.clone());
            info!(log, "fetched page";
                  "url" => url.as_str(),
                  "is_ok" => res.is_ok(),
                  "total" => res.as_ref().ok().and_then(|page| page.total_hint()).unwrap_or(0),
                  "duration" => comfy::Logable::from(watch.since()));
            match res {
                Err(Error::Http(ref err)) if !err.is_client_error() => {
                    if retries.next().is_some() {
                        warn!(
                            log,
                            "got an HTTP error that wasn't 4xx so retrying: {:#?}", err;
                        );
                        continue;
                    } else {
                        break res;
                    }
                }
                _ => (),
            };
            break res;
        };
        match res.as_ref().map(|page| page.items().is_empty()) {
            Ok(true) => break info!(log, "page was empty; peace out!"),
            _ => {
                let time = chrono::Utc::now(); // todo get this from the Date header in the response?
                let res = res.map(move |page| (page, time));
                let is_err = res.is_err();
                res_tx.send(res).expect("failed to send scrape page back");
                if is_err {
                    error_tolerance -= 1;
                    if error_tolerance == 0 {
                        error!(log, "Too many errors seen; quitting...");
                        break;
                    }
                }
            }
        }
    }
}

/// fuck
fn save_some_shit<'a, I>(
    mut it: I,
    pg: &postgres::Connection,
) -> Result<(u64, u64), postgres::Error>
where
    I: Iterator<Item = &'a db::Sampled<kraken::Stream>>,
{
    let tx = pg.transaction()?;
    //let mut param_count = db::SqlParams::new();
    //let sql = format!(
    //    "with c as ({} on conflict (_time, id) do nothing)
    //    {} on conflict (_time, id) do nothing",
    //    db::build_insert("kraken_channels", )
    //);
    // What a fucking mess ...
    let sql = r#"
        with _ as (
            insert into kraken_channels_canon (
                "_time",
                "id",
                "broadcaster_language",
                "broadcaster_software",
                "broadcaster_type",
                "created_at",
                "description",
                "display_name",
                "language",
                "logo",
                "is_mature",
                "name",
                "is_partner",
                "profile_banner",
                "profile_banner_background_color",
                "url",
                "video_banner"
            ) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)
            on conflict (id) do update
            set 
                "_time" = $1,
                "broadcaster_language" = EXCLUDED."broadcaster_language",
                "broadcaster_software" = EXCLUDED."broadcaster_software",
                "broadcaster_type" = EXCLUDED."broadcaster_type",
                "created_at" = EXCLUDED."created_at",
                "description" = EXCLUDED."description",
                "display_name" = EXCLUDED."display_name",
                "language" = EXCLUDED."language",
                "logo" = EXCLUDED."logo",
                "is_mature" = EXCLUDED."is_mature",
                "name" = EXCLUDED."name",
                "is_partner" = EXCLUDED."is_partner",
                "profile_banner" = EXCLUDED."profile_banner",
                "profile_banner_background_color" = EXCLUDED."profile_banner_background_color",
                "url" = EXCLUDED."url",
                "video_banner" = EXCLUDED."video_banner"
            where
                kraken_channels_canon."broadcaster_language" != EXCLUDED."broadcaster_language" OR
                kraken_channels_canon."broadcaster_software" != EXCLUDED."broadcaster_software" OR
                kraken_channels_canon."broadcaster_type" != EXCLUDED."broadcaster_type" OR
                kraken_channels_canon."created_at" != EXCLUDED."created_at" OR
                kraken_channels_canon."description" != EXCLUDED."description" OR
                kraken_channels_canon."display_name" != EXCLUDED."display_name" OR
                kraken_channels_canon."language" != EXCLUDED."language" OR
                kraken_channels_canon."logo" != EXCLUDED."logo" OR
                kraken_channels_canon."is_mature" != EXCLUDED."is_mature" OR
                kraken_channels_canon."name" != EXCLUDED."name" OR
                kraken_channels_canon."is_partner" != EXCLUDED."is_partner" OR
                kraken_channels_canon."profile_banner" != EXCLUDED."profile_banner" OR
                kraken_channels_canon."profile_banner_background_color" != EXCLUDED."profile_banner_background_color" OR
                kraken_channels_canon."url" != EXCLUDED."url" OR
                kraken_channels_canon."video_banner" != EXCLUDED."video_banner"
        ),
        __ as (
            insert into kraken_channels (
                "_time",
                "id",
                "updated_at",
                "followers",
                "status",
                "views"
            ) values ($1, $2, $18, $19, $20, $21)
            on conflict do nothing
        )
        insert into kraken_streams (
            "_time",
            "id",
            "average_fps",
            "channel",
            "broadcast_platform",
            "created_at",
            "delay",
            "game",
            "is_playlist",
            "video_height",
            "viewers"
        ) values ($1, $22, $23, $24, $25, $26, $27, $28, $29, $30, $31)
        on conflict do nothing
    "#;
    let statement = tx.prepare(sql)?;
    let res = it.try_fold(
        (0u64, 0u64),
        |res, sample| -> Result<(u64, u64), postgres::Error> {
            let stream = &sample.inner;
            let channel = &stream.channel;
            let values = &[
                &sample.time as &postgres::types::ToSql, // $1
                &channel.id as &postgres::types::ToSql,  // $2
                &channel.broadcaster_language as &postgres::types::ToSql, // $3
                &channel.broadcaster_software as &postgres::types::ToSql, // $4
                &channel.broadcaster_type as &postgres::types::ToSql, // $5
                &channel.created_at as &postgres::types::ToSql, // $6
                &channel.description as &postgres::types::ToSql, // $7
                &channel.display_name as &postgres::types::ToSql, // $8
                &channel.language as &postgres::types::ToSql, // $9
                &channel.logo as &postgres::types::ToSql, // $10
                &channel.is_mature as &postgres::types::ToSql, // $11
                &channel.name as &postgres::types::ToSql, // $12
                &channel.is_partner as &postgres::types::ToSql, // $13
                &channel.profile_banner as &postgres::types::ToSql, // $14
                &channel.profile_banner_background_color as &postgres::types::ToSql, // $15
                &channel.url as &postgres::types::ToSql, // $16
                &channel.video_banner as &postgres::types::ToSql, // $17
                //
                &channel.updated_at as &postgres::types::ToSql, // $18
                &channel.followers as &postgres::types::ToSql,  // $19
                &channel.status as &postgres::types::ToSql,     // $20
                &channel.views as &postgres::types::ToSql,      // $21
                //
                &stream.id as &postgres::types::ToSql,
                &stream.average_fps as &postgres::types::ToSql, // $22
                &stream.channel.id as &postgres::types::ToSql,  // $23
                &stream.broadcast_platform as &postgres::types::ToSql, // $24
                &stream.created_at as &postgres::types::ToSql,  // $25
                &stream.delay as &postgres::types::ToSql,       // $26
                &stream.game as &postgres::types::ToSql,        // $27
                &stream.is_playlist as &postgres::types::ToSql, // $28
                &stream.video_height as &postgres::types::ToSql, // $29
                &stream.viewers as &postgres::types::ToSql,     // $30
            ][..];
            // TODO consider using a query that returns a tuple of whether or not the channel and
            // stream were each inserted. However, I think typically they'd be the same ...
            let n = statement.execute(&values)?;
            let (inserted, duplicated) = res;
            Ok(if n > 0 {
                (inserted.checked_add(1).unwrap(), duplicated)
            } else {
                (inserted, duplicated.checked_add(1).unwrap())
            })
        },
    )?;
    tx.commit()?;
    Ok(res)
}
