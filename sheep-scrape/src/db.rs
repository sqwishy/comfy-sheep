pub trait Meme {
    //fn sql() -> &'static str;
    //fn execute(&postgers::Statement) -> postgres::Result<u64>;
    fn columns() -> &'static [&'static str];
    fn values(&self) -> Vec<&postgres::types::ToSql>;
}

/// A container for data that represent as the same resource across time. (As opposed to storing
/// only the last seen version of that resource.)
#[derive(Debug)]
pub struct Sampled<T> {
    pub time: chrono::DateTime<chrono::Utc>,
    pub inner: T,
}

/// Some memey thing for counting the parameters used when query building
#[derive(Debug)]
pub struct SqlParams(i32);

impl SqlParams {
    pub fn new() -> Self {
        SqlParams(0)
    }

    pub fn inc(&mut self) -> i32 {
        self.0 = self
            .0
            .checked_add(1)
            .expect("overflow? your query has way too many parameters my dude");
        self.0
    }
}

// /// shitty query building shit without escaping
// pub fn build_insert(into: &str, columns: &[&str], param_count: &mut SqlParams) -> String {
//     let mut columns_clause = String::new();
//     let mut values_clause = String::new();
//     if let Some((last, not_last)) = columns.split_last() {
//         for column in not_last {
//             columns_clause.push_str(column);
//             columns_clause.push(',');
//             values_clause.push_str(&format!("${}", param_count.inc()));
//             values_clause.push(',');
//         }
//         columns_clause.push_str(last);
//         values_clause.push_str(&format!("${}", param_count.inc()));
//     }
//     format!(
//         "insert into {} ({}) values ({})",
//         into, columns_clause, values_clause
//     )
// }

// pub fn build_upsert<T: Tabled>(param_count: &mut SqlParams) -> String {
//     let ins = build_insert::<T>(param_count);
//     let mut set_clause = String::new();
//     if let Some((last, not_last)) = T::columns().split_last() {
//         for column in not_last {
//             set_clause.push_str(&format!("{} = excluded.{}", column, column));
//             set_clause.push(',');
//         }
//         set_clause.push_str(&format!("{} = excluded.{}", last, last));
//     }
//     format!(
//         "{} on conflict ({}) do update set {}",
//         ins,
//         T::pkeys(),
//         set_clause
//     )
// }
