import logging
import typing
from datetime import datetime, timedelta, timezone
from urllib.parse import urlencode

import attr
from aiohttp import ClientSession

logger = logging.getLogger(__name__)


@attr.s
class TwitchAuth(object):
    # This comes from oauth2/token...
    access_token = attr.ib()
    refresh_token = attr.ib()
    expires_in = attr.ib()
    scope = attr.ib()
    # id_token = attr.ib()
    # token_type = attr.ib()

    # This comes from oauth2/userinfo...
    user_id = attr.ib()
    issued_at = attr.ib()
    preferred_username = attr.ib(default=None)
    picture = attr.ib(default=None)

    @property
    def expires_at(self):
        return self.issued_at + self.expires_in


@attr.s
class ComfyAuth(object):  # TODO rename to actoryish name
    pool = attr.ib()
    client_id = attr.ib()
    client_secret = attr.ib()
    redirect_uri = attr.ib()

    @property
    def start_oauth_url(self):
        start_qs = urlencode(
            {
                "client_id": self.client_id,
                "redirect_uri": self.redirect_uri,
                "response_type": "code",
                "scope": "openid chat:read",
                # TODO use nonce and claims?
            }
        )
        return "https://id.twitch.tv/oauth2/authorize?%s" % start_qs

    async def submit_oauth(self, code) -> TwitchAuth:
        auth = await self._request_token(
            {"grant_type": "authorization_code", "code": code}
        )

        async with self.pool.acquire() as con:
            twitch_id = await con.fetchval(
                """
                insert into twitch_oauths as auth
                    (twitch_id, access_token, refresh_token, issued_at, expires_in, scope)
                    values ($1, $2, $3, $4, $5, $6)
                on conflict (twitch_id) do update
                    set access_token = excluded.access_token,
                        refresh_token = excluded.refresh_token,
                        issued_at = excluded.issued_at,
                        expires_in = excluded.expires_in,
                        scope = excluded.scope
                returning twitch_id;
                """,
                auth.user_id,
                auth.access_token,
                auth.refresh_token,
                auth.issued_at,
                auth.expires_in,
                auth.scope,
            )
            return auth

    async def refresh_oauth(self, twitch_id) -> typing.Tuple[bool, TwitchAuth]:
        """Return true if refreshed, the TwitchAuth object"""
        async with self.pool.acquire() as con:
            refresh_token = await con.fetchval(
                "select refresh_token from twitch_oauths where twitch_id = $1",
                twitch_id,
            )

            params = {"grant_type": "refresh_token", "refresh_token": refresh_token}
            new_auth = await self._request_token(params)

            # In the event that this is called twice for the same token, we only
            # update the target record if the destination's issued_at is older than
            # ours.
            update = await con.fetchrow(
                """
                update twitch_oauths
                    set access_token = $2,
                        refresh_token = $3,
                        issued_at = $4,
                        expires_in = $5,
                        scope = $6
                    where twitch_id = $1 and issued_at < $4
                    returning twitch_id
                """,
                twitch_id,
                new_auth.access_token,
                new_auth.refresh_token,
                new_auth.issued_at,
                new_auth.expires_in,
                new_auth.scope,
            )
            return update is not None, new_auth

    async def _request_token(self, token_params) -> TwitchAuth:
        async with ClientSession() as session:
            # TODO handle 4xx errors ...
            async with session.post(
                "https://id.twitch.tv/oauth2/token",
                raise_for_status=True,
                params={
                    "client_id": self.client_id,
                    "client_secret": self.client_secret,
                    "redirect_uri": self.redirect_uri,
                    **token_params,
                },
            ) as resp:
                # TODO validate the access token thingy?
                # TODO also use a nonce or something
                data = await resp.json()
                logger.debug("oauth/token: %r", data)

                access_token = data["access_token"]
                expires_in = timedelta(seconds=data["expires_in"])
                refresh_token = data["refresh_token"]
                scope = data["scope"]

            async with session.get(
                "https://id.twitch.tv/oauth2/userinfo",
                raise_for_status=True,
                headers={"Authorization": "Bearer %s" % access_token},
            ) as resp:
                data = await resp.json()
                logger.debug("data from oauth/userinfo: %r", data)

                user_id = data["sub"]
                issued_at = datetime.fromtimestamp(data["iat"], timezone.utc)
                # This is not accurate ...
                # expires_at = datetime.fromtimestamp(data["exp"], timezone.utc)

            return TwitchAuth(
                access_token=access_token,
                refresh_token=refresh_token,
                scope=scope,
                user_id=data["sub"],
                issued_at=issued_at,
                expires_in=expires_in,
                preferred_username=data.get("preferred_username"),
                picture=data.get("picture"),
            )
