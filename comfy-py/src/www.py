"""OIDC Authorization Code Flow helper program thing.

https://dev.twitch.tv/docs/authentication/getting-tokens-oidc/#oidc-authorization-code-flow

If this receives oauth redirects from users it'll do the weird handshake thing and
add the obtained tokens into postgres.
"""

import argparse
import asyncio
import logging
from datetime import datetime, timedelta, timezone
from urllib.parse import urlencode

import asyncpg
import attr
import uvloop
from aiohttp import web

from comfy.auth import ComfyAuth

logger = logging.getLogger(__name__)


@attr.s
class ComfyAuthWWW(object):

    auth = attr.ib()
    refresh_queue = attr.ib()

    async def start_oauth(self, request):
        """Send the client to twitch..."""
        return web.HTTPSeeOther(self.auth.start_oauth_url)

    async def submit_oauth(self, request):
        """Receive a submitted OICD thing?"""
        try:
            code = request.query["code"]
        except KeyError:
            return web.HTTPBadRequest(text="`code` is required in query params")
        else:
            twitchauth = await self.auth.submit_oauth(code)
            name = twitchauth.preferred_username or twitchauth.user_id
            await refresh_queue.put((twitchauth.user_id, twitchauth.expires_at))
            return web.Response(text=f"welcome back {name}")

    # async def refresh_oauth(self, request):
    #    """Go and refresh the token?

    #    TODO this needs to be protected by some sort of auth, right?
    #    """
    #    try:
    #        twitch_id = request.query["twitch_id"]
    #    except KeyError:
    #        return web.HTTPBadRequest(text="`twitch_id` is required in query params")
    #    else:
    #        if await self.auth.refresh_oauth(twitch_id):
    #            return web.Response(text="woo hoo")
    #        else:
    #            return web.HTTPConflict()


async def index(request):
    return web.Response(
        content_type="text/html", text="""<a href="/login">Login via twitch.tv</a>"""
    )


async def prepare_db(*dbargs):
    return await asyncpg.create_pool(*dbargs)


async def schedule_refresh_for_existing_tokens(pool, queue):
    async with pool.acquire() as con:
        # Apparently we can refresh a token after it has expired ... isn't that neat
        for twitch_id, expires_at in await con.fetch(
            "select twitch_id, issued_at + expires_in from twitch_oauths"
        ):  # where expires_at > now()"):
            queue.put_nowait((twitch_id, expires_at))


async def run_queue_refreshes(comfy, queue):
    def schedule_refresh_for_auth(task):
        if not task.cancelled():
            _, auth = task.result()
            queue.put_nowait((auth.user_id, auth.expires_at))

    while True:
        twitch_id, expires_at = await queue.get()
        # refresh slightly before expiry
        expires_in = expires_at - datetime.now(timezone.utc) - timedelta(minutes=1)
        task = asyncio.create_task(refresh_token_in(comfy, twitch_id, expires_in))
        task.add_done_callback(schedule_refresh_for_auth)


async def refresh_token_in(comfy, twitch_id, delay: timedelta):
    if delay > timedelta(0):
        logger.info("refreshing %s in %s", twitch_id, delay)
        await asyncio.sleep(delay.total_seconds())
    else:
        logger.info("refreshing %s now", twitch_id)
    return await comfy.refresh_oauth(twitch_id)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--db", default="postgres:///sheep")
    parser.add_argument("--client-id")
    parser.add_argument(
        "--client-secret", default="client-secret", type=argparse.FileType("r")
    )
    parser.add_argument("--redirect-uri", default="http://localhost:8080/submit")
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)

    logger.debug("args: %s", args)

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()

    pool = loop.run_until_complete(prepare_db(args.db))
    refresh_queue = asyncio.Queue()

    comfy = ComfyAuth(
        pool=pool,
        client_id=args.client_id,
        client_secret=args.client_secret.read().strip(),
        redirect_uri=args.redirect_uri,
    )
    comfy_www = ComfyAuthWWW(comfy, refresh_queue)

    loop.create_task(schedule_refresh_for_existing_tokens(pool, refresh_queue))
    loop.create_task(run_queue_refreshes(comfy, refresh_queue))

    app = web.Application()
    app.add_routes([web.get("/", index)])
    app.add_routes([web.get("/login", comfy_www.start_oauth)])
    app.add_routes([web.get("/submit", comfy_www.submit_oauth)])
    # app.add_routes([web.get("/refresh", comfy_www.refresh_oauth)])
    web.run_app(app)
