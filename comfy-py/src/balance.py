"""OIDC Authorization Code Flow helper program thing.

https://dev.twitch.tv/docs/authentication/getting-tokens-oidc/#oidc-authorization-code-flow

If this receives oauth redirects from users it'll do the weird handshake thing and
add the obtained tokens into postgres.
"""

import asyncio
import logging
import pathlib
import operator
import functools

import asyncpg
import attr

import comfy.io
import toml
import uvloop

logger = logging.getLogger(__name__)


asyncpg__parse_hostlist = asyncpg.connect_utils._parse_hostlist

import urllib.parse


def motherfucking_connection_strings(hostlist, *what, **ever):
    """Oh my fucking god.

    > Note, however, that the slash is a reserved character in the hierarchical part of
    > the URI. So, to specify a non-standard Unix-domain socket directory, either omit
    > the host specification in the URI and specify the host as a parameter, or
    > percent-encode the path in the host component of the URI:

    https://www.postgresql.org/docs/10/libpq-connect.html
    """
    return asyncpg__parse_hostlist(urllib.parse.unquote(hostlist), *what, **ever)


asyncpg.connect_utils._parse_hostlist = motherfucking_connection_strings


def balance(channels, *targets):
    """
    All arguments should be lists and are modified in-place.

    >>> balance(['a', 'b'], ['b'], ['c'])
    (['b'], ['a'])
    >>> balance(['a', 'b'], ['b'], ['b'])
    (['b'], ['a'])
    >>> balance(['a', 'b', 'c', 'd'], ['a', 'b'], [])
    (['a', 'b'], ['c', 'd'])
    >>> balance(['b', 'c', 'd', 'e'], ['e', 'c'], ['b', 'd'])
    (['e', 'c'], ['b', 'd'])

    TODO support duplicates and avoid duplicates on the same target....

    # >>> balance(['a', 'b'] * 2, ['b'], ['c'])
    # (['b', 'a'], ['a', 'b'])
    # >>> balance(['a', 'b'] * 2, [], [], [])
    # (['a', 'b'], ['b'], ['a'])
    # >>> balance(['a', 'b', 'c'] * 2, ['a', 'b'], ['a'], ['b'])
    # (['a', 'b'], ['a', 'c'], ['b', 'c'])

    >>> balance(['a', 'b'], ['b'], ['e', 'd'])
    (['b'], ['a'])
    """
    assert targets

    # Remove targets' current channels from the complete channel list, or from the
    # target if they aren't in the complete channel list...

    # Build a mapping of channels keys to indexes? otherwise, testing if a channel is in
    # a list is fucking murder
    channel_idx = {v: e for e, v in enumerate(channels)}
    assert len(channel_idx) == len(channels), "duplicates not supported"

    is_not_none = functools.partial(operator.is_not, None)

    for target in targets:
        for e, current in enumerate(target):
            idx = channel_idx.pop(current, None)
            if idx is None:
                # current is a currently joined channel and it shouldn't be
                target[e] = None
            else:
                channels[idx] = None
        target[:] = filter(is_not_none, target)
    channels[:] = filter(is_not_none, channels)

    def move_into(dest_targets, *, until):
        """Move shards from channels[:until] into the targets evenly."""
        for e, target in enumerate(dest_targets):
            target.extend(channels[e : until : len(dest_targets)])

        del channels[:until]

    while channels:
        targets_lengths = set(map(len, targets))
        if len(targets_lengths) == 1:
            # trivial case, distribute across the targets normally
            move_into(targets, until=len(channels))
        else:
            # extend the shortest targets and to the length of the second shortest
            first, second, *_ = sorted(targets_lengths)
            targets_at_first = [t for t in targets if len(t) == first]
            stop = len(targets_at_first) * (second - first)
            move_into(targets_at_first, until=stop)

    return targets


def test_bench_balance_very_different_no_dupes(benchmark):
    new_channels = list(map(str, range(0, 300_000, 3)))
    targets = [list(map(str, range(s, 120_000, 4))) for s in range(4)]

    def setup():
        return ((new_channels[:], *[t[:] for t in targets]), {})

    benchmark.pedantic(balance, setup=setup, warmup_rounds=4, rounds=10)


def load_config_at(path):
    return toml.loads(open(path, "r").read())


def config_channels(config):
    try:
        return config["irc"]["channels"]
    except KeyError as e:
        raise KeyError("Missing %s when trying to read [irc].channels")


async def main(args):
    if not args.configs:
        logger.error("No configuration files specified to balance")
        raise SystemExit(1)

    configs = [load_config_at(path) for path in args.configs]
    configured_channels = [config_channels(config) for config in configs]

    db = await asyncpg.connect(args.db)

    sql = """
        select '#' || name, ks.viewers from kraken_channels_canon kcc
            inner join lateral (
                select viewers
                  from kraken_streams ks
                 where kcc.id = ks.channel
                   and ks.viewers >= $3
                   and ks._time > now() - $1::text::interval
                order by ks._time desc limit 1
            ) as ks
            on true
            where exists (select * from kraken_channels kc
                           where kcc.id = kc.id
                             and kc.views >= $2
                             and kc._time > now() - $1::text::interval)
            order by ks.viewers desc
    """
    vals = (args.since, args.min_channel_views, args.min_stream_viewers)

    if args.limit:
        sql += " limit $4"
        vals = (*vals, args.limit)

    if not args.write_changes:
        logger.warning("I am NOT going to write any changes...")

    logger.info("Fetching channel names...")

    results = await db.fetch(sql, *vals)
    active_channels = [r[0] for r in results]
    logger.info("Found %d channel names. %s...", len(results), active_channels[:20])

    logger.info("Balancing... %s", [len(c) for c in configured_channels])
    balance(active_channels, *configured_channels)
    logger.info("Balanced. %s", [len(c) for c in configured_channels])

    if args.write_changes:
        for path, channels in zip(args.configs, configured_channels):
            # Reload config files first in case they've changed since we last saw them.
            config = load_config_at(path)
            # Replace the recent config's channels list with the old config's balance's
            # channels
            config_channels(config)[:] = channels
            with comfy.io.safe_overwrite(path) as f:
                toml.dump(config, f)
            logger.info("Wrote %s", path)
    else:
        logger.info("Wrote nothing :)")
        # for data, conf, channels in zip(configs_data, configs, configured_channels):
        #    for line in difflib.unified_diff(
        #        data.splitlines(), toml.dumps(conf).splitlines()
        #    ):
        #        print(line)


if __name__ == "__main__":
    import argparse
    import pdb

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--db", default="postgres:///sheep", help="postgres connection string"
    )
    parser.add_argument(
        "--since", default="1 day", help="search for streams this amount of time ago"
    )
    parser.add_argument(
        "--limit",
        default=0,
        type=int,
        help="limit the number of results; zero is unlimited",
    )
    parser.add_argument(
        "--min-channel-views",
        type=int,
        default=10000,
        help="this string required for the formatter to show the default...",
    )
    parser.add_argument(
        "--min-stream-viewers",
        type=int,
        default=5,
        help="this string required for the formatter to show the default...",
    )
    parser.add_argument("--write-changes", default=False, action="store_true")
    parser.add_argument(
        "--profile",
        default=None,
        type=str,
        help="do profiling and write stats to a file at this path",
    )
    parser.add_argument("configs", nargs=argparse.REMAINDER, default=())
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)

    logger.debug("args: %s", args)

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()
    if args.profile:
        from cProfile import Profile

        profile = Profile()
        profile.enable()
    try:
        loop.run_until_complete(main(args))
    except (SystemExit, KeyboardInterrupt):
        raise
    finally:
        if args.profile:
            profile.disable()
            profile.dump_stats(args.profile)
