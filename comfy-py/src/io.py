import contextlib
import os
import tempfile


@contextlib.contextmanager
def safe_overwrite(path, mode="w"):
    """https://lwn.net/Articles/457672/
    
    >>> tmp = tempfile.NamedTemporaryFile()
    >>> open(tmp.name, 'w').write('foo')
    3
    >>> with safe_overwrite(tmp.name) as f:
    ...     f.write('bar')
    3
    >>> open(tmp.name).read()
    'bar'
    >>> with safe_overwrite(tmp.name) as f:
    ...     f.write('spam')
    ...     f.flush()
    ...     f.close()
    ...     raise Exception('boom')
    Traceback (most recent call last):
      ...
    Exception: boom
    >>> open(tmp.name).read()
    'bar'
    """
    dir_path = os.path.dirname(path)
    fd, tmp = tempfile.mkstemp(prefix=os.path.basename(path), dir=dir_path)
    try:
        with open(fd, mode, closefd=False) as f:
            yield f
            f.flush()
    except:
        os.unlink(tmp)
        raise
    else:
        os.rename(tmp, str(path))
        dir_fd = os.open(dir_path, os.O_RDONLY)
        try:
            os.fsync(dir_fd)
        finally:
            os.close(dir_fd)
    finally:
        if fd is not None:
            os.close(fd)
