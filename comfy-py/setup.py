import os.path

from setuptools import setup
from setuptools.config import read_configuration

conf_dict = read_configuration(os.path.join(os.path.dirname(__file__), "setup.cfg"))

setup(**conf_dict["metadata"], **conf_dict["options"])
