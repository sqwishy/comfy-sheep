Comfy Sheep
===========

This is a set of programs to record some data from `Twitch.tv <https://twitch.tv>`_ into
a `PostgreSQL <https://www.postgresql.org/>`_ database using `TimescaleDB
<https://www.timescale.com/>`_.

This document describes some goals & approaches taken for this project. As well as
instructions on building & running it.

There are some `pictures on the project wiki
<https://gitlab.com/sqwishy/comfy-sheep/wikis/pictures>`_.

This is GPL because nobody is going to use it anyway. (If you actually care about this
and don't like it, contact me.)

.. contents::

Overview
--------

Data is recorded from two places `Kraken`_ and `IRC`_.

Kraken
~~~~~~

Specifically, the sheep-scrape program queries the `Kraken
<https://dev.twitch.tv/docs/v5/>`_ API is scraped for all live streams. The streams
contain information about current live streams as well as the broadcaster's channel.
Each streams is stored in a hypertable (a table that is partitioned by TimescaleDB).
Each channel sampled is split into fields that tend to change often and those that
don't.  Fields that tend to change are always recorded in a hypertable. Otherwise, only
the latest "canonical" copy of the fields that don't change often are stored in a
regular table. However, a trigger will record an event in another table whenever this
"canonical" channel changes its name or becomes (un)partnered.

The point of this separation is mainly to avoid saving duplicate data when this record
rarely changes or changes in ways that we don't care about. A similar thing should maybe
be done of the streams as well, except that I'm not sure you'd gain much since the row
is already not too terribly wide and all you'd be left with is like the channel and
viewer count, which is like your entire index anyway.

So this lets us track streams' viewers and game over time. And channels' followers,
views, and live streams over time. Only the latest url, display name, logo, etc. for a
channel is recorded but name changes and parter changes are recorded and it's not
difficult to set up a table & trigger to track changes in other fields as well.

This corresponds to the :code:`sheep-scrape.service` systemd unit.

IRC
~~~

The sheep-chat program connects to Twitch's `IRC <https://dev.twitch.tv/docs/irc/>`_
service and records chat logs and events. It's really cool. There are some important
requirements I gave it.

- If it disconnects from IRC or fails to query PostgreSQL, reconnect with a backoff. For
  PostgreSQL, buffer messages instead of dropping them for as long as possible.
- Rate limit JOINs and PARTs differently. The Twitch IRC service will stop talking to
  you if you try to do stuff too often. But it lets you part more quickly than join.
- Handle some configuration changes while running. Chat loggers can be configured to
  join/part channels without reconnecting by modifying their configuration file and
  asking them to reload with SIGUSR1.  (This is how the sheep-balance service works.) I
  think they can also change their join/part rate limits without reconnecting.
- Multiple instances of the program may run and log to the same database. They should
  support having a precense in the same channels as other instances at the same time.
  (This bascially means having a uniqueness constraint in the database and doing nothing
  when that constraint conflicts with an insert.)

This corresponds to the :code:`sheep-chat@.service` systemd unit. [#]_

.. [#]
   *Bonus Meme*: This is an instanced service unit. The instance name :code:`%i`
   determines what configuration the program loads in
   :code:`/etc/comfy-sheep/sheep-chat-%i.toml`. I used numbers
   (:code:`sheep-chat@0.service`) but you can use other strings like the names of your
   friends, enemies, cats, etc.

comfy.balance
~~~~~~~~~~~~~

The runnable python module at :code:`comfy.balance` [#]_ queries the streams and
channels scraped by sheep-scrape to find candidate IRC channels to join based mainly on
viewership. It then looks at a non-empty set of sheep-chat configurations that specify
channel memberships for IRC loggers. It will rewrite the membership lists so that their
union strictly equals all the channels we want to log based on our database query. It
should try to do this in a way to minimize churn. For instance, it should never move
channels between configurations or otherwise ask two different loggers to together join
and part the same channel at the same time.

.. [#] The python modules are not installed to the system's site-packages, but instead
       to :code:`/usr/(local/)/lib/comfy-sheep/site-packages` or somewhere like that.
       The comfy python module, and its runtime dependencies may be found there.

This corresponds to the :code:`sheep-balance.service` systemd unit. This is WantedBy and
After :code:`sheep-scrape.service`. When it is enabled, this service will run after
:code:`sheep-scrape.service` completes.

sheep-vacuum
~~~~~~~~~~~~

The sheep-vacuum script runs vacuum on kraken_streams, kraken_channels, and
kraken_channels_canon.

Originally, it was to run vacuum analyze on every table which was important when I had
fewer tables and a problem with dead tuples by doing *very many* in-place updates. But
that's way to expensive and mostly stupid now.

A vacuum can update the `visibility map
<https://www.postgresql.org/docs/11/storage-vm.html>`_ so that index-only scans don't
need to access the heap and can be much faster. But figuring how how to tune the
autovacuum is probably a smarter way to go.

This corresponds to the :code:`sheep-vacuum.service` systemd unit. Nobody Wants it, but
if you enable it, it's After :code:`sheep-scrape.service` and Before
:code:`sheep-balance.service`. So it will run between those.

comfy-sheep.target
~~~~~~~~~~~~~~~~~~

:code:`comfy-sheep.target` is a target for this software collection. It's not required
that you do anything with it; it's supposed to be for convenience. If enabled, it should
enable the scraping & balance units as well as :code:`sheep-scrape.timer` (and you would
see that show up in :code:`systemctl list-timers`) [#]_. Enabling/starting should also
enable/start :code:`sheep-chat@{0,1,2,3}.service`.  You may not want that.

.. [#] If you don't see the timer in list-timers, try starting it or something. I don't
       know how to use timers in systemd. I usually just push all the buttons until
       something happens and then I don't know which button was the right one.

If the defaults do not match your desires, you can override them in systemd. My guess is
that you can change what other units are started when :code:`comfy-sheep.target` is
started by replacing :code:`Wants=` in the :code:`[Unit]` section of an override file
(see :code:`systemctl edit`). Similarly, replace :code:`Also=` in the :code:`[Install]`
section to control what this target enables.

That way, you can choose not to enable the vacuum job if you think it's stupid. But if
you want to change the number of chat loggers, you'll want to replace both lines, and
then that's like the whole file basically.

Development
-----------

I don't remember why I made this section. [#]_

.. [#] Note to self: Remember to increment versions properly, it makes you look like you
       kind of know what you're doing.

Use indexes. This project isn't Big :sup:`(tm)` Data :sup:`(c)` but the database is too
big to fit into memory. Indexes are great at making it so you don't have to read the
whole entire everything when you do a query. But the planner might not use them if it's
stupid or it will and you'll end up with heap fetches anyway because the visibility map
is a way that it shouldn't be. This can be solved by a vacuum analyze and regular vacuum
respectively, and that's part of there reason sheep-vacuum exists. Regardless, make
indexes and make sure you use them because they will make the difference between a query
taking seven seconds compared to seven hours.

TODO
~~~~

- All of the user interface.
- Finish writing this document.
- Migrations don't exist; the SQL for database initialization is packaged under
  somewhere like :code:`/usr/share/comfy-sheep/sql`.
- The chat logs are basically too big to query. The indexes for each chunk are like 8GB.

Releases
~~~~~~~~

*I would be nothing if not for my shell history and the incantations contained therein.*

Git tags for releases are not on master, a commit is made to include lock files on a
detached thingy and then that's tagged.

.. code::

   o---o---o---o---o---o---o  master
        \               \
         o  tag v1.2.3   o tag v1.2.4

   # To do this, make sure the .spec file contains the right version, and then...
   git checkout --detach HEAD
   # TODO is there a way to do this with git ls-files?
   git add -f {sheep-chat,sheep-scrape}/Cargo.lock
   git commit -va -m "Version 1.2.3"
   git tag -a v1.2.3 -m "Version 1.2.3"

Building
--------

This depends a bit on your goal. If you want to install on your host or prepare for
packaging, read on. Otherwise, if you don't care about the systemd services or anything
and just want to run stuff in the foreground, install comfy-py under a virtual
environment or with :code:`pip --user` to run Python things and use :code:`cargo build`
to build the rust binaries; you don't care about anything else in this section.

Requirements:

- I've used this with Fedora and openSUSE. It might run on similar operating systems.
- Since there is a lot of Rust, that and also Cargo are required.
- I think some rust programs depend on library headers for openssl and systemd (provided
  by systemd-devel and openssl-devel packages on Fedora IIRC).
- There is some Python. I think it all should run with Python 3.6+.
- `Ninja <https://ninja-build.org/>`_

Run :code:`bootstrap.sh` to generate a build.ninja file. This will be used if you run
:code:`ninja`. :code:`ninja build` will build rust binaries in release mode. :code:`sudo
ninja build` will install the systemd units and all their dependneices to various places
under :code:`/usr/local`.

Other The interesting targets include:

- :code:`build`: Compile the two Rust programs (sheep-scrape and sheep-chat) in release
  mode with cargo. This is the default target.
- :code:`install-bin`: Copy the Rust binaries and some shell scripts to the system
  (:code:`/usr/local/bin` by default and requires sudo in that case).
- :code:`install-systemd`: Prepare and write systemd unit files to the system
  (:code:`/usr/local/lib/systemd/system/` by default and requires sudo in that case).
- :code:`install-py`: Install the project's Python package to a project-specific
  site-package directory (:code:`/usr/local/lib/comfy-sheep/site-packages` by default and
  requires sudo in that case). This uses binary wheels where available so it will be
  specific to the system's Python runtime and not appropriate for source distribution.
- :code:`install`: Does all the above, after compiling the rust programs as an
  unprivileged user with :code:`ninja build`, you can meet the above install targets by
  building this target.
- :code:`install-py-src`: Download and bundle source distributions for the project's
  Python package and its dependencies under :code:`/usr/local/lib/comfy-sheep/py-src` by
  default. This is only something you would when packaging a source RPM. See the next
  section for more about that.

The paths above can be modified by setting environment variables when you run
:code:`bootstrap.sh`. See that file for details. The defaults are likely fine unless
you're preparing a build for `packaging an RPM`_.

Packaging an RPM
~~~~~~~~~~~~~~~~

Apart from the Python packages. The source RPM is just a bunch of binaries because I'm
dumb and lazy. To create the source RPM, run bootstrap.sh with the appropriate install
root and some prefix other than :code:`/usr/local`. This will give you a ninja file with
which you can build the :code:`install-for-rpm` target.

.. code::

   env INSTALL=/tmp/comfy-build PREFIX=/usr ./bootstrap.sh /dev/stdout \
      | ninja -vf /dev/stdin install-for-rpm

If that works, you can tar it up into where rpmbuild expects it and ask rpmbuild to
build a source RPM (or a regular RPM with :code:`rpmbuild -bb`). (Make sure the version
in the file path matches that in the spec file.)

.. code::

   # in fish
   tar -C /tmp/comfy-build \
      -czf ~/rpmbuild/SOURCES/comfy-sheep-(rpm --specfile misc/rpm/comfy-sheep.spec -q --queryformat "%{VERSION}").tar.gz .
   rpmbuild -bs misc/rpm/comfy-sheep.spec

That should create for you a source RPM at
:code:`~/rpmbuild/SRPMS/comfy-sheep-0.0.420-1.src.rpm`.

You can now send that to a Copr like https://copr.fedorainfracloud.org or somehow trick
https://build.opensuse.org into building an openSUSE Leap 15.0 RPM for you. The world is
your potato.

Installation
------------

Install the package. It should set up a comfy-sheep user and group and configuration
files.

If installing from source, after :code:`ninja build` run :code:`ninja install` to
install the binaries, systemd stuff, and python package to :code:`/usr/local` (by
default as configured in bootstrap.sh). Then do something like the following to set up
the user for the service.

.. code::

   # groupadd comfy-sheep --system
   # useradd comfy-sheep --system -g comfy-sheep \
      --comment "Comfy Sheep Service" \
      --home-dir /etc/comfy-sheep \
      --create-home --skel /dev/null

I've only tested this on Fedora 29 and openSUSE Leap 15.0. In "production" I used a
Google Compute VM which they call n1-highmem-2 (2 vCPUs, 13 GB memory). I was on a 500GB
standard persistent disk (not ssd) (formatted with XFS) for a while but increased it to
1000GB because the chat logs are much more plentiful than I anticipated. This is a
fairly conservative provisioning. It works for me. I chose Google Compute because I had
credit there. Things are pretty much IO bound. More memory would be good to keep
relevant/recent indexes/pages cached I guess. But I can't really say how well it would
scale otherwise.

Configuration
--------------

If you installed from a package, you can find configuration files in
:code:`/etc/comfy-sheep`. Modify those files appropriately. The env file is provided to
systemd units via EnvironmentFile, see `systemd.exec
<https://www.freedesktop.org/software/systemd/man/systemd.exec.html>`_ for more about
that.

If you are installing from the source, copy and modify :code:`misc/env` and
:code:`sheepchat/example-sheep-chat.toml` as you see fit.

PostgreSQL
~~~~~~~~~~

This applies do both package installations and manual installations from source.

PostgreSQL (10.6+ probably) is required. The TimescaleDB extension must be installed and
it's recommended that you use timescaledb-tune to configure PostgreSQL.

Create a database and user for comfy-sheep in PostgreSQL. I think sheep-vacuum only
needs to own the tables (as opposed to be a superuser). But sheep-vacuum is stupid
anyway and it's debatable whether you should enable it. But a superuser needs to set up
the schema because it runs `create extension
<https://www.postgresql.org/docs/11/sql-createextension.html>`_.

.. code::

   sudo -u postgres createuser comfy-sheep
   sudo -u postgres createdb comfy-sheep --owner comfy-sheep

Migrations aren't sorted out yet, the schema will be in the sql directory in a source
checkout, or at :code:`/usr/share/comfy-sheep/sql` from the package. After creating the
database above, run init.sql with psql to set up the schema. Something like:

.. code::

   sudo -u comfy-sheep psql -1f sql/init.sql

Retrospective
-------------

In another universe, this might be built into a platform for doing some sort of Twitch
analytics.
In this universe, the gap from what this is to what it needs to be so that it is useful
to broadcasters or consultants is too large for it to be practical for me to spend more
time on. I don't have enough knowledge of that domain or connections to those who do.

There are other sites that are already good at serving broadcaster information and
interesting time-series measurements. It *seems* to me that these sites do not offer
information that is as comprehensive and granular as I would imagine one would want if
one were either a broadcaster or someone who is invested in helping broadcasters
succeed. On the other hand, these folks uncharacteristically tend to struggle to read
basic time-series graphs, think that determining if a channel has been hosted must be
done by a person looking at view count over time, and won't reply to my email. And
indeed they are even successful (or successful enough) for that to work for them. So
what do I know. I am not a data scientist or an educator.

Maybe a good idea would be to paywall some information. Or to do something like run a
job that looks for trends and does like a weekly digest of some super interesting stuff
as a subscription. I don't know, I need a big brain to figure out what "super
interesting stuff" is...

*Everything could have been anything else and it would have just as much meaning.*

I did a neat thing with rust macros for mapping tags.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`IRC tags <https://ircv3.net/specs/extensions/message-tags.html>`_ look like
:code:`@aaa=bbb;ccc;example.com/ddd=eee :nick!ident@host.com PRIVMSG me :Hello`.

I wanted to define a mapping between key names and some type that would determine the
expected string format for the value being conveyed. I also wanted the decoding here to
fail if I encountered a tag that I didn't expect. 

This is a pretty innocuous mapping.  But I thought using some sort of map data structure
was overkill. I figured enumerating the keys and doing pattern matching on each would be
fine. I didn't benchmark anything here but I figure since the keys are known at
compile-time and limited to a couple dozen at most, doing (even perfect) hashing was not
appropriate and using a btree is pointless if I'm only enumerating the tags once or
twice.

I figured using a macro would let me define a mapping and generate code that applies it.
The macro output would iterate the list of tags once, finding fields and trying to
decode the values from strings into the expected type.

At first I had a macro that looked something like:

.. code::

   my_macro! {
      0 => "foo": i64,
      1 => "bar": bool,
      2 => "spam": Option<&str>,
   }

This evaluated to an anonymous function that took the tags as an argument and returned
a :code:`Result` for a tuple based on the mapping; in the above case,
:code:`(i64, bool, Option<&str>)`. I couldn't figure out how to count in macros so the
index of the tuple had to be passed explicitly. If you forgot, you got an unhelpful
error. I didn't like it.

I played around with this for a while, thought about just implementing it as
`deserializer for serde <https://serde.rs/impl-deserializer.html>`_ because that library
is cool. But that looked scary.

I rewrote the macro but in the end all that changed was that the macro executed
immediately instead of producing an anonymous function. This means you have to call it
from a function returning a result where the error implements :code:`std::convert::From`
for the tag decoding error. This worked perfectly in my case.  And the macro looks like:

.. code::

   my_macro! {
      tags,
      "foo" => foo: i64,
      "bar" => bar: bool,
      "spam" => spam: Option<&str>,
   }

This will assign those variables to values of their types from the specified keys.
The :code:`Option` type can be used for optional tags. A trait is used to implement
support for other types -- it looks a bit like `FromStr
<https://doc.rust-lang.org/std/str/trait.FromStr.html>`_ and I probably should have
tried to use that instead of making my own. Either :code:`&str` or :code:`()` can be
used if you don't care about the value of the tag but expect it anyway.

After doing this, I realized that I got type inference for free, which I had pained to
smuggle in intentionally earlier. So writing :code:`"foo" => foo: _,` worked if I went
to use :code:`foo` in some type-specific way like to initialize a struct. Wow; what a
time to be alive.

Asynchronous programming in rust is a fucking nightmare.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

I have these like 400 line monsters that I've spent weeks trying to clean up. I don't
know if there's some sort of trick/drug that everyone else knows about to either produce
good code or not go insane. But I would like some.

A lot of the futures API involves passing closures to iterator-like functions that
transform an stream of events or future. The borrow checker is a huge prick when using
closures like this. (A similar thing can happen with the normal iterator API but it's
less common/irritating.) If you want to share a reference to something in two different
closures, it counts as being borrowed. However, `"[a]t any given time, you can have
either one mutable reference or any number of immutable references."
<https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html>`_ So when you're
implementing your program as a collection of closures on streams and futures, they can't
mutably borrow something unless it is only available to one closure.

This difficulty is hard for me to accept since not all closures will be executed all the
time or in arbitrary sequences. If I use the futures API to say "do A, then do B, then
do either C if ok or D if err" where capital letters refer to closures, they can't
mutably borrow the same thing, even C & D which are mutually exclusive. Maybe this will
all go away with async/await.

Also, while I'm here, because of how traits and types can be implemented over other
traits, you can end up with a lot of nesting. For example, :code:`map` returns the 
:code:`Map` struct which implements the `Iterator`_ trait given another iterator and a
function taking the wrapped iterator's item returning a new item. This approach to
implementing a trait by wrapping the trait itself (or a version of it for some other
item type) is common in the futures API. But each level of nesting adds to the "concrete
type" and if you find yourself violating the language somewhere the compiler will
`vomit`_ at you with something like:

.. code::

   error[E0599]: no method named `inspect` found for type
   `futures::stream::Select<futures::FlattenStream<futures::future::Inspect<futures::MapErr<tokio::timer::Timeout<futures::MapErr<futures::Map<futures::stream::StreamFuture<futures::stream::SkipWhile<futures::stream::Select<futures::stream::MapErr<irc::client::ClientStream,
   fn(irc::error::IrcError) -> chat::Error {chat::Error::Irc}>,
   futures::stream::MapErr<futures::stream::Map<futures::stream::SkipWhile<futures::IntoStream<std::boxed::Box<dyn
   futures::Future<Item=(), Error=irc::error::IrcError>>>, [closure@src/chat.rs:246:41:
   246:58], std::result::Result<bool, irc::error::IrcError>>,
   [closure@src/chat.rs:247:51: 247:69]>, fn(irc::error::IrcError) -> chat::Error
   {chat::Error::Irc}>>, for<'r> fn(&'r irc::proto::Message) ->
   std::result::Result<bool, chat::Error> {chat::expect_motd}, std::result::Result<bool,
   chat::Error>>>, [closure@src/chat.rs:254:26: 254:55]>, [closure@src/chat.rs:255:30:
   255:52]>>, fn(tokio::timer::timeout::Error<chat::Error>) -> chat::Error {<chat::Error
   as std::convert::From<tokio::timer::timeout::Error<chat::Error>>>::from}>,
   [closure@src/chat.rs:258:30: 263:22 log:_, delays:_]>>,
   futures::stream::SkipWhile<futures::stream::AndThen<futures::stream::MapErr<streamext::Trickley<futures::sync::mpsc::UnboundedReceiver<chat::TrickledMessage<'_>>,
   &std::cell::RefCell<std::collections::VecDeque<chat::TrickledMessage<'_>>>>,
   [closure@src/chat.rs:269:50: 269:69]>, [closure@src/chat.rs:270:39: 270:109
   client:_], std::result::Result<(), chat::Error>>, [closure@src/chat.rs:271:41:
   271:58], std::result::Result<bool, chat::Error>>>` in the current scope
      --> src/chat.rs:274:22
       |
   274 |                     .inspect(|m| match &m.command {
       |                      ^^^^^^^
       |
       = note: the method `inspect` exists but the following trait bounds were not satisfied:
               `&mut
               futures::stream::Select<futures::FlattenStream<futures::future::Inspect<futures::MapErr<tokio::timer::Timeout<futures::MapErr<futures::Map<futures::stream::StreamFuture<futures::stream::SkipWhile<futures::stream::Select<futures::stream::MapErr<irc::client::ClientStream,
               fn(irc::error::IrcError) -> chat::Error {chat::Error::Irc}>,
               futures::stream::MapErr<futures::stream::Map<futures::stream::SkipWhile<futures::IntoStream<std::boxed::Box<dyn
               futures::Future<Item=(), Error=irc::error::IrcError>>>,
               [closure@src/chat.rs:246:41: 246:58], std::result::Result<bool,
               irc::error::IrcError>>, [closure@src/chat.rs:247:51: 247:69]>,
               fn(irc::error::IrcError) -> chat::Error {chat::Error::Irc}>>, for<'r>
               fn(&'r irc::proto::Message) -> std::result::Result<bool, chat::Error>
               {chat::expect_motd}, std::result::Result<bool, chat::Error>>>,
               [closure@src/chat.rs:254:26: 254:55]>, [closure@src/chat.rs:255:30:
               255:52]>>, fn(tokio::timer::timeout::Error<chat::Error>) -> chat::Error
               {<chat::Error as
               std::convert::From<tokio::timer::timeout::Error<chat::Error>>>::from}>,
               [closure@src/chat.rs:258:30: 263:22 log:_, delays:_]>>,
               futures::stream::SkipWhile<futures::stream::AndThen<futures::stream::MapErr<streamext::Trickley<futures::sync::mpsc::UnboundedReceiver<chat::TrickledMessage<'_>>,
               &std::cell::RefCell<std::collections::VecDeque<chat::TrickledMessage<'_>>>>,
               [closure@src/chat.rs:269:50: 269:69]>, [closure@src/chat.rs:270:39:
               270:109 client:_], std::result::Result<(), chat::Error>>,
               [closure@src/chat.rs:271:41: 271:58], std::result::Result<bool,
               chat::Error>>> : futures::Future`

.. _Iterator: https://doc.rust-lang.org/std/iter/struct.Map.html#impl-Iterator
.. _vomit: https://archive.is/parRY


I can't do name things.
~~~~~~~~~~~~~~~~~~~~~~~

I couldn't decide whether to use underscores or hyphens. Hyphenating filenames seems
wrong but it looks nicer for some reason. And everybody else is doing it too. So that
makes it okay. But really you can't be consistent because if you use hyphens it won't
work in some places, like :code:`extern crate` or Python imports. If you use underscores
it's different from everything else everywhere, especially in systemd units. Like you
just end up coming across as a total asshole. Like :code:`NetworkManager.service`. Why
are you capitalized? Nobody even likes you.

Speaking of PascalCase, I thought using it for table names was pretty kinky but table
names (I guess like most things) are case insensitive in SQL.  And psql won't tab
complete with capital letters. So I end up writing things like :code:`twitchroomstate`
and it looks stupid.

Also I was doing plural for table names and then stopped at the end. I don't even have a
good reason for this.

I really just should go back to bed.
