create extension if not exists "uuid-ossp";
create extension if not exists "timescaledb" cascade;

--drop schema if exists api cascade;
--create role grafana with password 'grafana';
create schema api;

\ir ./auth.sql
\ir ./sheep.sql
\ir ./chat.sql
