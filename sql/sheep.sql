create extension if not exists "uuid-ossp";
create extension if not exists "timescaledb" cascade;

-- helix and kraken data

create type twitch_broadcaster_role as enum ('partner', 'affiliate', '');
create type twitch_user_role as enum ('admin', 'global_mod', 'mod', 'staff', 'viewer', 'vip');
create type twitch_chatter_role as enum ('admin', 'broadcaster', 'global_mod', 'mod', 'staff', 'viewer', 'vip');

------------------------------------------------------------------
-- KRAKEN CHANNELS, nested inside of the streams stuff (see below)
-- TODO make a sample thing for this ... and try to put columns that are not regularly
-- updated in the canonical table.
create table kraken_channels_canon (
    _time           timestamptz(0) not null,
    id              bigint      primary key,
    created_at      timestamptz not null,
    url             text        not null,           -- URL
    name            text        not null,
    display_name    text        not null,
    broadcaster_language text   not null default '',
    broadcaster_software text   not null default '', -- usually the
    broadcaster_type text       not null default '', -- emtpy string?
    description     text        not null,
    language        text        not null default '',
    logo            text,
    profile_banner  text,                           -- URL
    profile_banner_background_color text not null default '',
    video_banner    text,                           -- URL
    is_mature       boolean     not null,
    is_partner      boolean     not null
);

create index on kraken_channels_canon (created_at desc);
create index on kraken_channels_canon (name);
-- NOTE: I have found that this can very much speed up sheep-balance when it is matching
-- channel ids to names when a heap lookup is costly? This was much more the case on my
-- spinning platters of rust rather than in production on whatever fancy cloud VM
-- storage is being used. Performance profiles were pretty different between the two
-- environments; maybe because of differences in storage mediums and their random access
-- times?
create index on kraken_channels_canon (id, name);

-- name changes

create table channel_name_changes (
    -- id          serial          not null primary key,
    channel     bigint          not null references kraken_channels_canon,
    ts          timestamptz(0)  not null,
    old         text            not null,
    new         text            not null
);

select create_hypertable('channel_name_changes', 'ts', chunk_time_interval => interval '7 day');

create function record_channel_name_change()
    returns trigger
as $$
begin
    insert into channel_name_changes
                (channel, ts,        old,      new)
         values (new.id,  new._time, old.name, new.name);
    return new;
end;
$$ language plpgsql;

create trigger record_channel_name_change
    after update of name on kraken_channels_canon
    for each row
        when (new.name != old.name)
        execute procedure record_channel_name_change();

-- WITH RECURSIVE renames(name) AS (
--     (VALUES ($1)
--   UNION ALL
--      SELECT change.new
--        FROM renames
--           , channel_name_changes change
--       WHERE change.old = renames.name)
-- SELECT name
--   FROM renames;

-- SELECT *
--   FROM channel_name_changes l
--   JOIN channel_name_changes r
--    ON l.new = r.old
-- WHERE l.ts < r.ts

-- is_partner changes

create table channel_is_partner_changes (
    channel     bigint          not null references kraken_channels_canon,
    ts          timestamptz(0)  not null,
    is_partner  boolean         not null
);

select create_hypertable('channel_is_partner_changes', 'ts', chunk_time_interval => interval '7 day');

create or replace function record_channel_is_partner_change()
    returns trigger
as $$
begin
    insert into channel_is_partner_changes
                (channel, ts,        is_partner)
         values (new.id,  new._time, new.is_partner);
    return new;
end;
$$ language plpgsql;

create trigger record_channel_is_partner_change
    after update of is_partner on kraken_channels_canon
    for each row
        when (new.is_partner != old.is_partner)
        execute procedure record_channel_is_partner_change();

-- channel time-series stuffs

create table kraken_channels (
    _time           timestamptz(0) not null,
    id              bigint      not null references kraken_channels_canon,
    updated_at      timestamptz not null,
    followers       bigint      not null,
    views           bigint      not null,
    status          text        not null,

    primary key (_time, id)
);

select create_hypertable('kraken_channels', '_time', chunk_time_interval => interval '1 day');

-- Needed for sheep-balance.
-- NOTE: It might be a very good idea to use a "views" constraint here. This will filter
-- out around 80% of rows from the index, reducing its size considerably. Depending on
-- your hardware (available cache/IO?) this may provide a significant performance by
-- reducing the time taken to read into cache and the amount of cache consumed.
-- (see also similar index on kraken_streams below)
create index on kraken_channels ("id", "_time" desc, "views" desc); -- where "views" >= 10000;

-- this might be useless
-- create index on kraken_channels ("_time" desc, "views" desc);
-- create index on kraken_channels ("_time" desc, "followers" desc);

-------------------------------------------------------------------
-- KRAKEN STREAM - https://dev.twitch.tv/docs/v5/reference/streams/
-- these are sampled through time, so they have a time column ...
create table kraken_streams (
    _time           timestamptz(0) not null,
    id              bigint      not null,
    average_fps     real,
    channel         bigint      not null references kraken_channels_canon,
    broadcast_platform text     not null default '',
    created_at      timestamptz not null,
    delay           real,
    game            text        not null default '',
    is_playlist     boolean     not null,
    video_height    integer     not null,
    viewers         bigint      not null,

    primary key (_time, id)
    -- fks to hypertables are not supported ...
    -- foreign key (_time, channel) references kraken_channels (_time, id)
);

select create_hypertable('kraken_streams', '_time', chunk_time_interval => interval '1 day');

create index on kraken_streams ("id");

-- This is important for sheep-balance:
-- See the NOTE above on a similar index on kraken_channels. Using viewers >= 10 removes
-- a bit more than 80% of the rows from the index (or a bit under 75% at >= 5). Smaller
-- indexes are faster to read and easier to hold in cache. This may matter for some
-- hardware profiles.
-- (tfw half your database size is indexes ...)
create index on kraken_streams ("channel", "_time" desc, "viewers" desc); -- where "viewers" >= 5;

------------------- TIME THINGY
-- These were invoked by postgrest somehow for some reason that I don't remember

create function ts_to_epoch_ms(ts timestamptz) returns bigint as $$
    select round(extract(epoch from ts) * 1000)::bigint
$$ language sql;


create function _time_ms(kraken_streams) returns bigint as $$
    select ts_to_epoch_ms($1._time);
$$ language sql;


create function _time_ms(kraken_channels) returns bigint as $$
    select ts_to_epoch_ms($1._time);
$$ language sql;


-- from helix

-- TODO separate the login/display_name and view_count into time series and broadcaster
-- type ... basically the whole thing?
create table helix_users (
    id                  integer primary key,
    broadcaster_type    twitch_broadcaster_role not null,
    description         text    not null,
    display_name        text    not null,
    email               text,
    login               text    not null,
    offline_image_url   text,
    profile_image_url   text,
    role                twitch_user_role not null, -- called type in twitch ...
    view_count          integer not null
);

-- a sample of who is a chatter in some channel at some time
create table twitch_chatters (
    _time      timestamptz(0)   not null,
    channel_id integer          not null,
    viewer_id  integer          not null,
    role       twitch_chatter_role not null,

    unique (_time, channel_id, viewer_id)
);

select create_hypertable('twitch_chatters', '_time', chunk_time_interval => interval '1 day');

-- This is used by shep-in.py, which knows how to parse overrustlelogs into this
-- structure.
-- From IRC we get some interesting tags like:
--   msg-id, msg-param-months, msg-param-sub-plan-name, msg-param-sub-plan, room-id
-- see also: https://dev.twitch.tv/docs/irc/tags/#privmsg-twitch-tags
create table sheeple (
  -- tag: id
  id          uuid             not null,
  -- tag: tmi-sent-ts
  time        timestamptz      not null,
  -- message.command.args.0
  channel     text             not null,
  -- tag: room-id
  channel_id  integer          not null,
  -- tag: badges
  -- badges      text             not null,
  -- badges      hstore           not null,
  -- ordinarily tag: login, tag: user-id
  -- for gifts  tag: msg-param-recipient-id, tag: msg-param-recipient-user-name
  subscriber  text             not null,
  subscriber_id integer        not null,
  -- tag: msg-param-months
  months      integer          not null
              check (months >= 0),
  -- tag: login, tag: user-id
  gifted_by   text,
  gifted_by_id integer,
  -- message.command.suffix
  message     text             not null,
  -- tag: msg-param-sub-plan-name
  plan        integer          not null,
  plan_name   text             not null
);

select create_hypertable('sheeple', 'time', chunk_time_interval => interval '1 day');

create unique index on sheeple (id, time);
create index on sheeple (channel_id);
