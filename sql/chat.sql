create extension if not exists "uuid-ossp";
create extension if not exists "timescaledb" cascade;

create table if not exists twitch_irc_purgatory (
    id          bigserial primary key,
    timestamp   timestamptz not null default now(),
    message     text not null,
    -- unused, maybe used in the future? otherwise get rid of it
    error       text not null
);

----------
-- PRIVMSG

create table TwitchChat (
    id          uuid            not null,
    ts          timestamp       not null,
    channel     bigint          not null references kraken_channels_canon,
    author      bigint          not null,
    message     text            not null,
    bits        integer,
    -- Some of this user stuff, including the display-name, could be denormalized into
    -- another time-series table that only inserts rows with changes ...
    badges      jsonb,
    badge_info  jsonb,

    -- we need this to prevent duplicates
    primary key (ts, id)
);

select create_hypertable('TwitchChat', 'ts', chunk_time_interval => interval '1 day');
create index on TwitchChat (channel, ts desc);

------------
-- USERNOTICE & msg-id is some sort of subscription thingy

create table TwitchSubscription (
    id          uuid        not null,
    ts          timestamp   not null,
    channel     bigint      not null references kraken_channels_canon,
    subscriber  bigint      not null,
    gifted_by   bigint,
    months      integer,    -- is this even necessary?
    message     text        not null,
    plan        text        not null,

    primary key (ts, id)
);

select create_hypertable('TwitchSubscription', 'ts', chunk_time_interval => interval '7 day');
create index on TwitchSubscription (channel, ts);

------------
-- ROOMSTATE

create table TwitchRoomState (
    -- Should _time have a timezone? It isn't from the twitch message, we make it up ...
    _time           timestamptz(0) not null,
    channel         bigint         not null references kraken_channels_canon,
    delay           integer,
    followers_only  integer,
    is_emote_only   boolean,
    is_r9k          boolean,
    is_subs_only    boolean,

    -- Avoid duplicates? This is hard since these messages don't have much to uniquely
    -- identify them across different clients ...
    -- This is really sketchy because we could receive different sets of updates for the
    -- same channel at the same time?
    primary key (channel, _time)
);

select create_hypertable('TwitchRoomState', '_time', chunk_time_interval => interval '7 days');

-------------
-- HOSTTARGET

-- This is only for when hosts start ...
create table TwitchHost (
    _time       timestamptz(0) not null,
    host        bigint         not null references kraken_channels_canon,
    target      bigint         not null references kraken_channels_canon,
    viewers     integer,

    primary key (host, _time)
);

select create_hypertable('TwitchHost', '_time', chunk_time_interval => interval '7 days');
