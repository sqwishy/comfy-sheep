--create table users (
--    id           uuid primary key default uuid_generate_v4(),
--    name         text not null default ''
--);

create table twitch_oauths (
    twitch_id     text primary key,

    access_token  text not null,
    refresh_token text not null,
    issued_at     timestamptz not null,
    expires_in    interval second not null,
    scope         text[] not null
);
