/// reminder to sqwishy; this hasn't been moved into comfy-rust because you hate futures and don't
/// want comfy-rust to depend on that library ...
use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::time::{Duration, Instant};

use futures::{Future, Poll, Stream};

use tokio::timer::Delay;

pub trait Trickle {
    /// if this is called rate, shouldn't it be something over a duration?
    fn moment_of_silence(&self) -> Duration;
}

/// it is ugly that we require a RefCell ... I'd really just want to use BorrowMut but it doesn't
/// seem like there's a way to have a RefCell implement that trait, or any other trait that would
/// allow us to receive both a VecDeque or &mut VecDeque or &RefCell<VecDeque> ... :(
pub struct Trickley<S, V>
where
    S: Stream,
    S::Item: Trickle,
    V: Borrow<RefCell<VecDeque<S::Item>>>,
{
    stream: S,
    vec: V,
    pause: Option<Delay>,
}

impl<S, V> Stream for Trickley<S, V>
where
    S: Stream,
    S::Item: Trickle,
    V: Borrow<RefCell<VecDeque<S::Item>>>,
{
    type Item = S::Item;
    type Error = S::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        use futures::Async;

        let upstream_result = loop {
            // Drain upstream ... returning on error, breaking if NotReady or empty
            match self.stream.poll() {
                res @ Ok(Async::NotReady) => break res,
                res @ Ok(Async::Ready(None)) => break res,
                Ok(Async::Ready(Some(item))) => {
                    self.vec.borrow().borrow_mut().push_back(item);
                }
                Err(poop) => return Err(poop),
            }
        };

        let is_waiting = if let Some(ref mut delay) = self.pause {
            match delay.poll().expect("fuck timer errors") {
                Async::Ready(()) => false,
                Async::NotReady => true,
            }
        } else {
            false
        };

        if is_waiting {
            Ok(Async::NotReady)
        } else {
            match self.vec.borrow().borrow_mut().pop_front() {
                // We have nothing queued, so return our upstream's poll()
                None => upstream_result,
                Some(item) => {
                    self.pause
                        .replace(Delay::new(Instant::now() + item.moment_of_silence()));
                    Ok(Async::Ready(Some(item)))
                }
            }
        }
    }
}

pub trait StreamExt: Stream {
    fn trickle<V>(self, vec: V) -> Trickley<Self, V>
    where
        Self: Sized,
        Self::Item: Trickle,
        V: Borrow<RefCell<VecDeque<Self::Item>>>,
    {
        Trickley {
            stream: self,
            vec,
            pause: None,
        }
    }
}

impl<T: ?Sized> StreamExt for T where T: Stream {}

#[test]
fn test_rate_limit() {
    use futures::stream;
    use std::time::{Duration, Instant};
    use tokio_core::reactor;

    impl Trickle for u64 {
        fn moment_of_silence(&self) -> Duration {
            Duration::from_millis(*self)
        }
    }

    let s = stream::iter_ok(vec![10, 20, 0, 10])
        .map_err(|_: ()| ())
        .trickle(RefCell::new(VecDeque::default()))
        .map(|i| (Instant::now(), i))
        .collect();

    let arrival_delay_pairs = reactor::Core::new().unwrap().run(s).unwrap();
    match arrival_delay_pairs.as_slice() {
        &[(_, 10), (_, 20), (_, 0), (_, 10)] => (),
        _ => panic!("{:?}", arrival_delay_pairs),
    };
    let delays = arrival_delay_pairs
        .as_slice()
        .windows(2)
        .filter_map(|window| match window {
            &[(former, _), (latter, _)] => Some(latter - former),
            _ => None,
        });
    assert!(delays
        .map(comfy::duration_to_f64)
        .zip(
            vec![10, 20, 0, 10]
                .into_iter()
                .map(Duration::from_millis)
                .map(comfy::duration_to_f64)
        )
        // almost equal?
        .all(|(actual, expected)| (actual - expected).abs() < 3.0));
}
