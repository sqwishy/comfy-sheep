extern crate chrono;
extern crate core;
extern crate futures;
extern crate irc;
extern crate itertools;
extern crate postgres;
extern crate rand;
extern crate signal_hook;

#[macro_use]
extern crate clap;

#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_journald;
extern crate slog_term;

extern crate tokio;
extern crate tokio_core;
extern crate toml;
extern crate uuid;

#[macro_use]
extern crate failure;

#[macro_use]
extern crate quick_error;

extern crate serde;
#[macro_use]
extern crate serde_derive;
#[cfg(test)]
#[macro_use]
extern crate serde_json;

extern crate comfy;

use clap::Arg;
use slog::Drain;

use chrono::prelude::{DateTime, Utc};
use irc::proto::Message as IrcMessage;

use std::fmt;
use std::str::FromStr;
use std::thread;
use std::time::Duration;

mod persist;
mod twitch;

pub enum Seriousness {
    ScanOnly,
    DryRun,
    Commit,
    CommitAndDelete,
}

impl FromStr for Seriousness {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "scan-only" => Ok(Seriousness::ScanOnly),
            "dry-run" => Ok(Seriousness::DryRun),
            "commit" => Ok(Seriousness::Commit),
            "commit-and-delete" => Ok(Seriousness::CommitAndDelete),
            _ => Err("Nope".into()),
        }
    }
}

fn main() {
    let matches = clap::App::new("sheep-chat")
        .author("somebody@froghat.ca")
        .version(env!("CARGO_PKG_VERSION"))
        .args(&[
            Arg::from_usage("--db [val] 'pg connection string'").required(true),
            Arg::from_usage("-v... 'Log and complain when backfilling'"),
            Arg::from_usage("--halt-on-error"),
            Arg::from_usage("--journal 'Log to the journal'"),
            // TODO backfill should be a different executable ...
            Arg::from_usage("--mode <MODE> 'Instead of running a chat agent, go through unhandled messages and try to load them into the appropriate table'")
                .default_value("scan-only")
                .possible_values(&["scan-only", "dry-run", "commit", "commit-and-delete"]),
            Arg::from_usage("--limit <LIMIT> 'Number of messages to process'").required(false),
            Arg::from_usage("--cursor <CURSOR> 'Start processing at this message id'").default_value("0"),
            Arg::from_usage("--order <ORDER> 'Enumerate records ascending or descending'")
                .default_value("asc")
                .possible_values(&["asc", "desc"]),
            Arg::from_usage("--pause <PAUSE> 'Wait for this many ms before each batch; a crude form of rate-limiting to reduce database contention'")
                .default_value("0"),
        ])
        .get_matches();

    let log = {
        if matches.is_present("journal") {
            let drain = slog_journald::JournaldDrain.ignore_res();
            slog::Logger::root(drain, o!())
        } else {
            let decorator = slog_term::TermDecorator::new().build();
            let drain = slog_term::CompactFormat::new(decorator).build().fuse();
            let drain = slog_async::Async::new(drain).chan_size(2048).build().fuse();
            slog::Logger::root(drain, o!())
        }
    };

    match main_backfill(&log, matches) {
        Ok(()) => (),
        Err(e) => error!(log, "Something terrible happened: {}", e),
    }
}

pub fn main_backfill(log: &slog::Logger, matches: clap::ArgMatches) -> Result<(), postgres::Error> {
    let connstr: String = value_t_or_exit!(matches, "db", String);
    let pg = persist::pg_connect(&connstr).expect("where is the database?");

    use std::sync::atomic::{AtomicBool, Ordering};
    use std::sync::Arc;
    let stop = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::SIGINT, Arc::clone(&stop))?;
    signal_hook::flag::register(signal_hook::SIGTERM, Arc::clone(&stop))?;
    // TODO make this a separate binary for the love of god...
    // Since we run under the same name as sheep-chat, we look like it and may be told to reload
    // our configuration. So we have to handle/ignore it ...
    let literally_nothing = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::SIGUSR1, Arc::clone(&literally_nothing))
        .expect("install signal handlers");

    let (should_insert, should_commit, should_delete) =
        match value_t_or_exit!(matches, "mode", Seriousness) {
            Seriousness::ScanOnly => (false, false, false),
            Seriousness::DryRun => (true, false, false),
            Seriousness::Commit => (true, true, false),
            Seriousness::CommitAndDelete => (true, true, true),
        };

    let halt_on_error = matches.is_present("halt-on-error");
    let verbosity: u64 = matches.occurrences_of("v");
    let pause = Duration::from_millis(value_t_or_exit!(matches, "pause", u64));
    let order: String = value_t_or_exit!(matches, "order", String);
    let mut cursor: i64 = value_t_or_exit!(matches, "cursor", i64);
    let total_limit: Option<u64> = if matches.is_present("limit") {
        Some(value_t_or_exit!(matches, "limit", u64))
    } else {
        None
    };

    assert!(order == "asc" || order == "desc");
    let query = format!(
        r#"
         SELECT id
              , timestamp
              , message
           FROM twitch_irc_purgatory
          WHERE id {} $1
       ORDER BY id {}
          LIMIT $2
          FOR UPDATE SKIP LOCKED
        "#,
        if order == "asc" { ">=" } else { "<=" },
        &order,
    );

    const BATCH_LIMIT: u32 = 5000; // Making this time-based would be cool ...

    #[derive(Debug, Default)]
    struct Stats {
        seen: u64,
        inserts: u64,
        dupes: u64,
        ignores: u64,
        failures: u64,
    }

    let mut stats = Stats::default();

    info!(log, "starting backfill...");

    if !should_commit {
        info!(log, "This is a dry run; I'm NOT committing anything...");
    } else {
        if !should_delete {
            info!(log, "I will NOT delete backfilled messages.");
        }
    };

    loop {
        if stop.load(Ordering::Relaxed) {
            break;
        }

        if pause > Duration::from_secs(0) {
            thread::sleep(pause);
            if stop.load(Ordering::Relaxed) {
                break;
            }
        }

        let limit = if let Some(n) = total_limit.as_ref() {
            let remaining = n
                .checked_sub(stats.seen)
                .expect("somehow we saw too many rows");
            std::cmp::min(remaining as u32, BATCH_LIMIT)
        } else {
            BATCH_LIMIT
        };

        debug_assert!(limit <= BATCH_LIMIT);

        let tx = pg.transaction()?;

        let rows = {
            let mut q = if should_insert {
                Some(persist::Queries::for_pg(&tx))
            } else {
                None
            };

            let del = if should_commit && should_delete {
                tx.prepare(r#"DELETE FROM twitch_irc_purgatory WHERE id = $1"#)?
                    .into()
            } else {
                None
            };

            // We're intentionally using `for update` here even if we're not committing?
            let rows = tx.query(&query, &[&cursor, &(limit as i64)])?;

            for row in &rows {
                if stop.load(Ordering::Relaxed) {
                    break; // stop early on a signal
                }

                cursor = row.get(0);
                let timestamp: DateTime<Utc> = row.get(1);
                let irc_str: String = row.get(2);

                stats.seen += 1;

                let irc_msg = IrcMessage::from_str(&irc_str);
                let affect = match &irc_msg {
                    Ok(irc_msg) => backfill_one(&irc_msg, timestamp, q.as_mut()),
                    Err(err) => Affect::Failed(format!(
                        "funny string couldn't be parsed as an IRC message: {}\nmessage was: {}",
                        err,
                        irc_str.trim(),
                    )),
                };
                match &affect {
                    Affect::Inserted(_) => stats.inserts += 1,
                    Affect::Duplicate(_) => stats.dupes += 1,
                    Affect::Ignored(_) => stats.ignores += 1,
                    Affect::Unimplemented(_) | Affect::Failed(_) => stats.failures += 1,
                };
                if halt_on_error {
                    match &affect {
                        Affect::Unimplemented(_) | Affect::Failed(_) => {
                            error!(log, "Stopping early from failure: {}", affect; "cursor" => cursor);
                            // overload guy due to laziness TODO
                            stop.store(true, Ordering::Relaxed);
                            break;
                        }
                        _ => (),
                    }
                }
                match &affect {
                    Affect::Inserted(_) if verbosity >= 4 => {
                        info!(log, "{}", affect; "cursor" => cursor)
                    }
                    Affect::Duplicate(_) if verbosity >= 3 => {
                        info!(log, "{}", affect; "cursor" => cursor)
                    }
                    Affect::Ignored(_) if verbosity >= 2 => {
                        info!(log, "{}", affect; "cursor" => cursor)
                    }
                    Affect::Unimplemented(_) if verbosity >= 1 => {
                        warn!(log, "{}", affect; "cursor" => cursor)
                    }
                    Affect::Failed(_) => error!(log, "{}", affect; "cursor" => cursor),
                    _ => (),
                }
                if let Some(del) = del.as_ref() {
                    match &affect {
                        Affect::Inserted(_) | Affect::Duplicate(_) | Affect::Ignored(_) => {
                            match del.execute(&[&cursor]) {
                                Ok(_) => (),
                                Err(err) => {
                                    error!(log, "Failed deleting backfilled row"; "cursor" => cursor);
                                    Err(err)?;
                                }
                            }
                        }
                        _ => (),
                    }
                }
            }

            rows
        };

        if should_commit {
            tx.commit()?;
        }

        info!(log, "{:#?}", stats);

        if (rows.len() as u32) < BATCH_LIMIT
            || (total_limit.is_some() && Some(stats.seen) >= total_limit)
        {
            info!(log, "Stopping; ran out of rows or reached requested limit");
            break;
        }
    }

    info!(log, "cursor was {}", cursor);

    Ok(())
}

#[derive(Debug)]
enum Affect<'a> {
    Inserted(twitch::Event<'a>),
    Duplicate(twitch::Event<'a>),
    Ignored(&'a IrcMessage),
    Unimplemented(&'a IrcMessage),
    Failed(String),
}

impl<'a> fmt::Display for Affect<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Affect::Inserted(event) => write!(f, "inserted:\n{:#?}", event),
            Affect::Duplicate(event) => write!(f, "duplicate:\n{:#?}", event),
            Affect::Ignored(irc_msg) => write!(f, "explicitly ignored:\n{:#?}", irc_msg),
            Affect::Unimplemented(irc_msg) => write!(f, "not yet supported:\n{:#?}", irc_msg),
            Affect::Failed(explanation) => write!(f, "{}", explanation),
        }
    }
}

fn backfill_one<'a>(
    irc_msg: &'a IrcMessage,
    timestamp: DateTime<Utc>,
    q: Option<&mut persist::Queries>,
) -> Affect<'a> {
    let event = match twitch::as_twitch_event(&irc_msg, timestamp) {
        Ok(Some(event)) => event,
        Ok(None) => {
            return Affect::Ignored(&irc_msg);
        }
        Err(twitch::event::Error::Unimplemented) => {
            return Affect::Unimplemented(&irc_msg);
        }
        Err(err) => {
            return Affect::Failed(format!(
                "couldn't read event: {}\nmessage was: {:#?}",
                err, irc_msg
            ));
        }
    };

    if let Some(q) = q {
        match q.insert_event(&event) {
            Ok(0) => Affect::Duplicate(event),
            Ok(_) => Affect::Inserted(event),
            //Err(err) => Err((event, err)),
            Err(err) => Affect::Failed(format!(
                "couldn't insert event: {}\nevent was: {:#?}",
                err, event
            )),
        }
    } else {
        // when not inserting, basically just lie and pretend we inserted
        Affect::Inserted(event)
    }
}
