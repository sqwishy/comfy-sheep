use futures::future;
use futures::future::Future;
use futures::sync::mpsc::Receiver;
use futures::Stream;

use irc::proto::Message as IrcMessage;

use chrono::prelude::{DateTime, Utc};

use std::iter::Iterator;
use std::time;

use tokio::runtime::current_thread;
use tokio::timer;

use crate::twitch;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "internal timer bugger: {}", 0)]
    Timer(#[cause] timer::Error),
    #[fail(display = "postgres error: {}", 0)]
    Pg(#[cause] postgres::Error),
}

impl From<postgres::Error> for Error {
    fn from(e: postgres::Error) -> Error {
        Error::Pg(e)
    }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Config {
    pub db: String,
    pub process_events: Option<bool>,
}

#[derive(Debug)]
pub enum Message {
    Reconfigure(Config),
    Irc(IrcMessage),
    // Like a ping but without a pong, measure latency but not rtt ...
    Poke(time::Instant),
}

impl Into<Message> for IrcMessage {
    fn into(self) -> Message {
        Message::Irc(self)
    }
}

trait Delayable {
    fn delay(&mut self, duration: time::Duration) -> Result<(), Error>;
}

impl Delayable for current_thread::Runtime {
    fn delay(&mut self, duration: time::Duration) -> Result<(), Error> {
        let timer = timer::Delay::new(time::Instant::now() + duration).map_err(Error::Timer);
        self.block_on(timer)
    }
}

#[derive(Debug)]
pub struct Fief {
    pub log: slog::Logger,
    pub rx: Receiver<Message>,
    pub cfg: Config,
}
impl Fief {
    pub fn run_forever(self: Self) -> Result<(), Error> {
        use future::Either;

        let Fief { log, rx, mut cfg } = self;

        const MAX_PENDING: usize = 512;
        const MAX_DELAY: time::Duration = time::Duration::from_millis(3500);

        let mut insert_me = Vec::<_>::with_capacity(MAX_PENDING);
        let mut runtime = current_thread::Runtime::new().unwrap();
        let mut pg = pg_connect_loop(
            &log,
            &mut comfy::new_delays(),
            &mut runtime,
            cfg.db.as_ref(),
        )?;
        let mut flush_timer: Box<Future<Item = (), Error = ()>> = Box::new(future::empty());

        let mut rx = rx.into_future();

        loop {
            let res = runtime.block_on(rx.select2(flush_timer));
            match res {
                Ok(Either::A(((None, _), _timer))) => break Ok(()),
                Ok(Either::A(((Some(message), more_rx), timer))) => {
                    rx = more_rx.into_future();
                    flush_timer = timer;
                    match message {
                        Message::Poke(time_sent) => {
                            debug!(log, "Poke!";
                                   "pending" => insert_me.len(),
                                   "delay" => comfy::Logable::from(time::Instant::now() - time_sent));
                            continue;
                        }
                        Message::Reconfigure(new_cfg) => {
                            cfg = new_cfg;
                            continue;
                        }
                        Message::Irc(irc) => {
                            if insert_me.is_empty() {
                                // Start a flush timer...
                                let deadline = time::Instant::now() + MAX_DELAY;
                                flush_timer = Box::new(
                                    timer::Delay::new(deadline)
                                        .map(|_| ())
                                        .map_err::<_, ()>(|e| {
                                            panic!("stupid timer shit I can't be bothered to handle: {}", e)
                                        }),
                                );
                            }
                            insert_me.push((irc, Utc::now()));
                            if insert_me.len() < insert_me.capacity() {
                                continue;
                            }
                        }
                    }
                }
                Ok(Either::B((_timer, more_rx))) => {
                    // timeout! do flush...
                    rx = more_rx;
                }
                Err(_) => unimplemented!(),
            };

            // flush!
            let mut retry_delays = comfy::new_delays();
            let start = time::Instant::now();
            // This is toggled while looping so that if we're having database troubles we'll
            // alternate between trying to store events and falling back to storing purgatory
            // messages ...
            let mut insert_as_events = true;

            let (end, num_inserts) = loop {
                let insert_as_events = if cfg.process_events == Some(false) {
                    false
                } else {
                    let negated = !insert_as_events;
                    std::mem::replace(&mut insert_as_events, negated)
                };

                // TODO If this fails, we may want to repeat it with these messages. We might
                // instead want to write them to the purgatory place ...
                match insert_irc_messages(&log, &pg, insert_me.iter(), insert_as_events) {
                    Ok(num_inserts) => {
                        break (time::Instant::now(), num_inserts);
                    }
                    Err(e) => {
                        error!(log, "Failed to store irc messages: {}", e);
                        pg = pg_connect_loop(
                            &log,
                            &mut retry_delays,
                            &mut runtime,
                            cfg.db.as_str(),
                        )?;
                    }
                }
            };

            let duration = end - start;
            info!(log, "Committed records."; "count" => num_inserts, "duration" => comfy::Logable::from(duration));
            insert_me.clear();
            flush_timer = Box::new(future::empty());
        }
    }
}

fn insert_irc_messages<'a, I>(
    log: &slog::Logger,
    pg: &postgres::Connection,
    messages: I,
    make_an_effort: bool,
) -> Result<u64, postgres::Error>
where
    I: Iterator<Item = &'a (IrcMessage, DateTime<Utc>)>,
{
    let mut inserts = 0;
    let tx = pg.transaction()?;
    {
        // For some stupid reason I can't get q to drop without using a block like this ...
        let mut q = Queries::for_pg(&tx);
        for (message, timestamp) in messages {
            if make_an_effort {
                match twitch::as_twitch_event(message, *timestamp) {
                    Ok(Some(event)) => match q.insert_event(&event) {
                        Ok(n) => {
                            inserts += n;
                            continue;
                        }
                        Err(e) => warn!(log, "error storing twitch event: {}", e),
                    },
                    Ok(None) => continue,
                    Err(twitch::event::Error::Unimplemented) => (),
                    Err(e) => warn!(log, "error reading twitch event: {}", e),
                };
            }
            inserts += q.insert_to_purgatory(&timestamp, &message.to_string())?;
        }
    };
    tx.commit()?;
    return Ok(inserts);
}

pub fn pg_connect(connstr: &str) -> Result<postgres::Connection, postgres::Error> {
    postgres::Connection::connect(connstr, postgres::TlsMode::None)
}

fn pg_connect_loop<I>(
    log: &slog::Logger,
    delays: &mut I,
    runtime: &mut current_thread::Runtime,
    connstr: &str,
) -> Result<postgres::Connection, Error>
where
    I: Iterator<Item = time::Duration>,
{
    loop {
        match postgres::Connection::connect(connstr, postgres::TlsMode::None) {
            Ok(pg) => break Ok(pg),
            Err(e) => match delays.next() {
                Some(delay) => {
                    warn!(
                        log,
                        "Failed to connect to pg, retrying in {:?}: {}", delay, e
                    );
                    runtime.delay(delay)?;
                    continue;
                }
                None => return Err(Error::Pg(e)),
            },
        }
    }
}

#[derive(Debug)]
struct LazyStatement<'a>(String, Option<postgres::stmt::Statement<'a>>);

impl<'a> LazyStatement<'a> {
    fn new(sql: String) -> Self {
        LazyStatement(sql, None)
    }

    fn get_statement<C>(&mut self, pg: &'a C) -> Result<&postgres::stmt::Statement, postgres::Error>
    where
        C: postgres::GenericConnection,
    {
        match self.1 {
            None => self.1 = Some(pg.prepare(self.0.as_str())?),
            _ => (),
        };
        Ok(self.1.as_ref().unwrap())
    }
}

// TODO turn this into something cooler and do fancy-pantsy stuff like counting inserts in here
// instead of in calling code?
#[derive(Debug)]
pub struct Queries<'a> {
    pg: &'a postgres::transaction::Transaction<'a>,
    ins_purgatory: LazyStatement<'a>,
    ins_channel_message: LazyStatement<'a>,
    ins_host: LazyStatement<'a>,
    ins_roomstate: LazyStatement<'a>,
    ins_subscription: LazyStatement<'a>,
}

impl<'a> Queries<'a> {
    pub fn for_pg(pg: &'a postgres::transaction::Transaction) -> Self {
        Queries {
            pg,
            ins_purgatory: LazyStatement::new(
                r#"
                INSERT INTO twitch_irc_purgatory (timestamp, message, error)
                     VALUES ($1, $2, '')
                "#.to_owned(),
            ),
            ins_channel_message: LazyStatement::new(
                r#"
                INSERT INTO TwitchChat (id, ts, channel, author, message, badges, badge_info)
                                VALUES ($1, $2, $3, $4, $5, $6, $7)
                           ON CONFLICT (ts, id) DO NOTHING
                "#
                .to_owned(),
            ),
            // TODO try to look up renames in channel_name_changes?
            // otherwise, handle the error more gracefully? I think the transaction gets merced on
            // an error
            ins_host: LazyStatement::new(
                r#"
                       WITH foo(_time, host, target, viewers)
                         AS (SELECT $1::timestamptz, $2, $3, $4::integer)
                INSERT INTO TwitchHost (_time, host, target, viewers)
                     SELECT foo._time, host.id, target.id, foo.viewers from foo
                  LEFT JOIN kraken_channels_canon host
                         ON host.name = foo.host
                  LEFT JOIN kraken_channels_canon target
                         ON target.name = foo.target
                ON CONFLICT (_time, host) DO NOTHING
                "#
                .to_owned(),
            ),
            ins_roomstate: LazyStatement::new(
                r#"
                INSERT INTO TwitchRoomState (_time, channel, delay, followers_only, is_emote_only, is_r9k, is_subs_only)
                                     VALUES ($1, $2, $3, $4, $5, $6, $7)
                                ON CONFLICT (_time, channel) DO NOTHING
                "#
                .to_owned(),
            ),
            ins_subscription: LazyStatement::new(r#"
                INSERT INTO TwitchSubscription (id, ts, channel, subscriber, gifted_by, months, message, plan)
                                        VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
                                   ON CONFLICT (ts, id) DO NOTHING
            "#.to_owned())
        }
    }

    // pub fn query_irc_log(&mut self, cursor: i64, limit: i64) -> Result<bool, postgres::Error> {
    // }

    pub fn insert_to_purgatory(
        &mut self,
        timestamp: &DateTime<Utc>,
        irc_string: &str,
    ) -> Result<u64, postgres::Error> {
        let stmt = self.ins_purgatory.get_statement(self.pg)?;
        stmt.execute(&[&timestamp, &irc_string])
    }

    pub fn insert_event(&mut self, event: &twitch::Event) -> Result<u64, postgres::Error> {
        match event {
            twitch::Event::ChannelMessage {
                id,
                ts,
                channel_id,
                text,
                user_id,
                badges,
                badge_info,
                ..
            } => {
                let stmt = self.ins_channel_message.get_statement(self.pg)?;
                stmt.execute(&[
                    &id,
                    &ts as &postgres::types::ToSql,
                    &channel_id,
                    &user_id,
                    &text,
                    &badges,
                    &badge_info,
                ])
            }

            twitch::Event::Subscription {
                id,
                ts,
                channel_id,
                subscriber,
                gifted_by,
                months,
                message,
                plan,
            } => {
                let stmt = self.ins_subscription.get_statement(self.pg)?;
                stmt.execute(&[
                    &id,
                    &ts,
                    &channel_id,
                    &subscriber,
                    &gifted_by,
                    &months,
                    &message,
                    &plan,
                ])
            }

            twitch::Event::Host {
                timestamp,
                host,
                target,
                viewers,
            } => {
                // use a savepoint so that if this fails we don't heck up our underlying
                // transaction
                // TODO this is a huge pain though because if we fail to start a transaction,
                // that's considerably more serious than if we fail to execute the statement ... I
                // guess that's kind of a problem with hiding away this statement cacheing garbage
                // TODO We should probably differentiate between something recoverable and not?
                let sp = self.pg.transaction()?;
                let res = {
                    let mut meme = Queries::for_pg(&sp);
                    let stmt = meme.ins_host.get_statement(&sp)?;
                    stmt.execute(&[&timestamp, &host, &target, &viewers])?
                };
                sp.commit()?;
                Ok(res)
            }

            twitch::Event::RoomState {
                timestamp,
                channel_id,
                delay,
                followers_only,
                is_emote_only,
                is_r9k,
                is_subs_only,
            } => {
                let stmt = self.ins_roomstate.get_statement(self.pg)?;
                stmt.execute(&[
                    &timestamp,
                    &channel_id,
                    &delay,
                    &followers_only,
                    &is_emote_only,
                    &is_r9k,
                    &is_subs_only,
                ])
            }
        }
    }
}
