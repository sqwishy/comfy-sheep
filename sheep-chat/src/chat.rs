// TODO this should be separate files :)
use futures::sync::mpsc::{self, Receiver, SendError, Sender};
use futures::{future, Future, Sink, Stream};

use irc::client::ext::ClientExt;
pub use irc::client::prelude::Config as IrcConfig;
use irc::client::{Client, IrcClient, PackedIrcClient};
use irc::error::IrcError;
use irc::proto::caps::Capability;
use irc::proto::response::Response;
use irc::proto::{Command, Message as IrcMessage};

use std::cell::RefCell;
use std::collections::BTreeSet;
use std::convert::Into;
use std::iter::FromIterator;
use std::time;
use std::time::Duration;

use tokio::prelude::FutureExt;
use tokio::timer;
use tokio_core::reactor;

use crate::streamext::{StreamExt, Trickle};

use crate::persist;

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Config {
    #[serde(flatten)]
    client: IrcConfig,
    #[serde(default)]
    rate_limits: RateLimits,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct RateLimits {
    join: u64,
    part: u64,
}

impl Default for RateLimits {
    fn default() -> Self {
        Self {
            join: 100,
            part: 10,
        }
    }
}

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "some irc fuckup")]
    Irc(#[cause] IrcError),
    #[fail(display = "internal timer bugger")]
    Timer(#[cause] timer::Error),
    #[fail(display = "timeout")]
    Timeout,
    #[fail(display = "authentication didn't work")]
    BadAuth,
    #[fail(display = "lost pg receiver")]
    LostRecevier(#[cause] SendError<persist::Message>),
    #[fail(display = "command & control channel closed; administrative shutdown?")]
    LostCnc,
}

impl From<IrcError> for Error {
    fn from(e: IrcError) -> Error {
        Error::Irc(e)
    }
}

impl From<SendError<persist::Message>> for Error {
    fn from(e: SendError<persist::Message>) -> Error {
        Error::LostRecevier(e)
    }
}

impl<T: std::convert::Into<Error>> From<timer::timeout::Error<T>> for Error {
    fn from(e: timer::timeout::Error<T>) -> Error {
        if e.is_inner() {
            e.into_inner().unwrap().into()
        } else if e.is_timer() {
            Error::Timer(e.into_timer().unwrap())
        } else {
            assert!(e.is_elapsed());
            Error::Timeout
        }
    }
}

#[derive(Debug)]
pub enum Message {
    Reconfigure(Config),
}

#[derive(Debug)]
pub struct Fief {
    pub log: slog::Logger,
    pub pg: Sender<persist::Message>,
    pub cnc: Receiver<Message>,
    pub cfg: Config,
}

#[derive(Debug)]
enum Meme {
    Retry(String),
    WaitForReconfigure(String),
    GiveUp(String),
}

impl From<Error> for Meme {
    fn from(e: Error) -> Meme {
        match e {
            Error::Irc(e) => match e {
                e @ IrcError::Io(_)
                | e @ IrcError::Tls(_)
                | e @ IrcError::InvalidMessage { .. }
                | e @ IrcError::PoisonedLog
                | e @ IrcError::PingTimeout
                | e @ IrcError::NoUsableNick => {
                    Meme::Retry(format!("(Hopefully) temporary IRC error?: {}", e))
                }
                IrcError::InvalidConfig { .. } => {
                    Meme::WaitForReconfigure(format!("IRC config problem: {}", e))
                }
                _ => Meme::GiveUp(format!("IRC derped out?: {}", e)),
            },
            Error::Timeout => Meme::Retry(
                "Something timed out, but I'm not sure what because my author is an idiot.".into(),
            ),
            Error::BadAuth => Meme::WaitForReconfigure("Authorization didn't work.".into()),
            e @ Error::Timer(_) | e @ Error::LostRecevier(_) => {
                Meme::GiveUp(format!("Something derped out?: {}", e))
            }
            e @ Error::LostCnc => Meme::GiveUp(format!("{}", e)),
        }
    }
}

impl Fief {
    pub fn run_forever(self: Self) -> Result<(), Error> {
        let mut core = reactor::Core::new().unwrap();

        let Fief {
            log,
            mut pg,
            cfg,
            cnc,
        } = self;

        let cfg = RefCell::new(cfg);

        let mut delays = comfy::new_delays();
        let mut cnc_task = cnc.map_err::<Error, _>(|_| unreachable!()).into_future();

        //let debug_print_message =
        //    |message: &IrcMessage| debug!(log, "recv > {}", message.to_string().trim_end());

        // It would actually probably not be a bad idea to group together the mutable state, that
        // is required to exist (and be correct) at the end and the beginning of each loop, into
        // its own object. It's something like:
        //   struct RunState<F>
        //   where
        //       F: Stream<Item = (Option<Message>, Stream<Item = Message, Error = Error>), Error = Error>
        //           + Sized,
        //   {
        //       pg: Sender<persist::Message>,
        //       cfg: IrcConfig,
        //       core: reactor::Core,
        //       cnc_task: F,
        //   }
        // But I don't know how to implement that because it uses a trait? So instead there's a big
        // loop that we just break out of ... :)

        // In lieu of using <expr>? ...
        macro_rules! break_on_err {
            ($x:expr) => {
                match $x {
                    Ok(r) => r,
                    Err(e) => break Meme::from(Error::from(e)),
                };
            };
        }

        loop {
            match loop {
                let mut client_cfg = (*cfg.borrow()).clone();
                let cfg_channels = client_cfg.client.channels.take();
                let PackedIrcClient(client, run_client) = {
                    // This block exists to drop connect because I think it owns the reference to
                    // config, we can't allow that because we need to assign to it later ...
                    let connect =
                        break_on_err!(IrcClient::new_future(core.handle(), &client_cfg.client))
                            .map_err(Error::Irc)
                            .timeout(time::Duration::from_secs(45))
                            .map_err::<_, Error>(timer::timeout::Error::into);
                    break_on_err!(core.run(connect))
                };

                info!(log, "Connected, identifying...");

                break_on_err!(client.send_cap_req(&[
                    // https://dev.twitch.tv/docs/irc/guide/#twitch-irc-capabilities
                    // A bunch of sick memes tbh
                    Capability::Custom("twitch.tv/tags"),
                    // Includes hosts, subscriptions, removing messages retroactively
                    Capability::Custom("twitch.tv/commands"),
                ]));
                break_on_err!(client.identify());

                // fuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuck
                // trickle_tx is for sending messages that should be rate-limited, _rx for
                // receiving those messages after the rate limit is applied. messages waiting to be
                // sent are stored in the trickle_queue
                let trickle_queue = RefCell::new(std::collections::VecDeque::default());

                let (trickle_tx, trickle_rx) = mpsc::unbounded::<TrickledMessage>();
                // enqueue inital joins into rate-limit
                let trickle_tx = cfg_channels
                    .into_iter()
                    .flatten()
                    .map(|channel| Command::JOIN(channel, None, None).into())
                    .map(|join| TrickledMessage(&cfg, join))
                    .fold(trickle_tx, |trickle_tx, m| {
                        trickle_tx
                            .unbounded_send(m)
                            .expect("failed queing up initial joins...");
                        trickle_tx
                    });

                let stream = client
                    .stream()
                    .map_err::<Error, _>(Error::Irc)
                    .select(
                        // This tucks the run_client future (which I think is responsible for sending out
                        // messages or just doing ping pongs and shit) into the future for the irc message
                        // stream. This uses select and so, for streams, when one ends the select continues
                        // polling on the remaining until it ends. So if run_client ends, skip_while drops
                        // the message and it the main IrcMessage stream may remain open, depending on what
                        // the library's implementation happens to do. Alternatively, we could replace
                        // skip_while with .and_then(|_: ()| Err(Error::Something)) to short circuit the
                        // main stream ... TODO
                        run_client
                            .into_stream()
                            .skip_while(|_: &()| Ok(true))
                            .map::<IrcMessage, _>(|_| unreachable!())
                            .map_err::<Error, _>(Error::Irc),
                    )
                    // Takes a stream and returns a wrapped stream of IRC messages that will read and drop
                    // the messages until it sees the last MOTD
                    .skip_while(expect_motd)
                    .into_future() // resolves into a pair with ... some shit ifk?
                    .map(|(_last_motd, stream)| stream)
                    .map_err(|(e, _): (Error, _)| e) // TODO can we really drop the stream here?
                    .timeout(time::Duration::from_secs(5))
                    .map_err(Error::from)
                    .inspect(|_| {
                        info!(log, "We identified, horray!");
                        // TODO maybe don't do this, maybe only if we've been connected for a
                        // little while?
                        delays = comfy::new_delays();
                    })
                    .flatten_stream()
                    .select(
                        /////////////////// k i l l m e \\\\\\\\\\\\\\\\\\\
                        trickle_rx
                            .trickle(&trickle_queue)
                            .map_err::<Error, _>(|()| unreachable!())
                            .and_then(|TrickledMessage(_, m)| client.send(m).map_err::<Error, _>(Error::Irc))
                            .skip_while(|_: &()| Ok(true))
                            .map::<IrcMessage, _>(|_| unreachable!()),
                    )
                    .inspect(|m| match &m.command {
                        Command::JOIN(channel, _, _) => {
                            info!(log, "join"; "channel" => channel, "pending" => trickle_queue.borrow().len());
                        }
                        Command::PART(channel, _) => {
                            info!(log, "part"; "channel" => channel, "pending" => trickle_queue.borrow().len());
                        }
                        _ => (),
                    });

                // Periodically poke the other end ...
                // TODO seriously consider chanining the above with an error so that if the
                // IrcMessage stream ends, we don't hang, because I think selecting on streams like
                // this will run until they all complete ... whereas if either of these end then
                // something is abnormal. See the todo above also ...
                let interval = timer::Interval::new_interval(time::Duration::from_millis(1000))
                    .map_err(Error::Timer)
                    .map(persist::Message::Poke);
                let stream = stream.map(persist::Message::Irc).select(interval);

                let mut irc_stream = stream.into_future();

                // Handle Command & Control and IRC messages for as long as possible
                break loop {
                    match core.run(cnc_task.select2(irc_stream)) {
                        Ok(future::Either::A(((None, more_cnc), _))) => {
                            cnc_task = more_cnc.into_future(); // this is kind of silly ...
                            break Error::LostCnc.into();
                        }
                        Ok(future::Either::A(((Some(msg), more_cnc), more_irc))) => {
                            cnc_task = more_cnc.into_future();
                            match msg {
                                Message::Reconfigure(new) => {
                                    // TODO holy shit please figure out how to move this into a
                                    // function ...
                                    info!(log, "New configuration!");
                                    let old = &cfg.replace(new);
                                    let new = cfg.borrow();
                                    if *new != *old {
                                        //debug_assert!(new != *cfg.borrow());
                                        // Match on a tuple of whether the password, channels, or
                                        // limits are changed ...
                                        match (
                                            old.client.password != new.client.password,
                                            old.client.channels != new.client.channels,
                                            old.rate_limits != new.rate_limits,
                                        ) {
                                            (false, false, false) => {
                                                // something else changed, we don't really know if its
                                                // fine to not restart ... so restart ...
                                                info!(log, "I don't know what changed so I'm going to reconnect...");
                                                if let Err(e) = client.send_quit("brb") {
                                                    info!(
                                                        log,
                                                        "Error trying to send quit message: {}", e
                                                    );
                                                };
                                                break Meme::Retry(
                                                    "New configuration requires restart.".into(),
                                                );
                                            }
                                            (_, false, _) => {
                                                // If the password or the rate limits change, we can
                                                // ignore them ...
                                                info!(log, "Nothing important changed...");
                                            }
                                            (_, true, _) => {
                                                info!(log, "Updating channel membership...");
                                                // todo, this doesn't use the current client state, so
                                                // if we failed to join something or whatever, then we
                                                // will remain out of sync ...
                                                let (part, join) = diff_membership(
                                                    &old.client.channels,
                                                    &new.client.channels,
                                                );

                                                if !part.is_empty() {
                                                    info!(log, "Parting"; "channels" => -(part.len() as i64));
                                                }
                                                if !join.is_empty() {
                                                    info!(log, "Joining"; "channels" => (join.len() as i64));
                                                }

                                                part.into_iter()
                                                    .map(|channel| {
                                                        Command::PART(channel.to_string(), None)
                                                    })
                                                    .chain(join.into_iter().map(|channel| {
                                                        Command::JOIN(
                                                            channel.to_string(),
                                                            None,
                                                            None,
                                                        )
                                                    }))
                                                    .map(|cmd| TrickledMessage(&cfg, cmd.into()))
                                                    .try_for_each(|tricked| {
                                                        trickle_tx.unbounded_send(tricked)
                                                    })
                                                    .expect(
                                                        "failed to queue up a channel part/join",
                                                    );
                                            }
                                        }
                                    }
                                }
                            }
                            irc_stream = more_irc;
                        }
                        Ok(future::Either::B(((maybe_msg, more_irc), more_cnc))) => {
                            cnc_task = more_cnc;
                            if let Some(msg) = maybe_msg {
                                // Can't use break_on_err! here (last I checked) since, if we fail
                                // to send, we don't get the Sender back, and I guess either the
                                // compiler doesn't realize that breaking shouldn't retry here I
                                // guess?
                                pg = core.run(pg.send(msg.into()))?;
                            } else {
                                break Meme::Retry("Lost IRC client message stream?".into());
                            }
                            irc_stream = more_irc.into_future();
                        }
                        // Error from the cnc task.
                        Err(future::Either::A(((e, _), _))) => return Err(e),
                        // Error from the irc stream.
                        Err(future::Either::B(((e, _), more_cnc))) => {
                            cnc_task = more_cnc;
                            break e.into();
                        }
                    };
                };
            } {
                Meme::GiveUp(reasons) => {
                    warn!(log, "Giving up because: {}", reasons);
                    return Ok(());
                }
                Meme::Retry(reasons) => {
                    let delay = match delays.next() {
                        Some(delay) => delay,
                        None => {
                            error!(
                                log,
                                "Couldn't figure out how long to wait for, giving up ..."
                            );
                            return Ok(());
                        }
                    };
                    warn!(log, "Retrying IRC in {:?} because: {}", delay, reasons);
                    let mut timer =
                        timer::Delay::new(time::Instant::now() + delay).map_err(Error::Timer);
                    loop {
                        match core.run(cnc_task.select2(timer)) {
                            Ok(future::Either::A(((None, _), _))) => {
                                return Err(Error::LostCnc);
                            }
                            Ok(future::Either::A(((Some(msg), more_cnc), more_timer))) => match msg
                            {
                                Message::Reconfigure(new_cfg) => {
                                    timer = more_timer;
                                    cnc_task = more_cnc.into_future();
                                    if *cfg.borrow() != new_cfg {
                                        info!(log, "New configuration!");
                                        cfg.replace(new_cfg);
                                        break; // Retry now!
                                    }
                                }
                            },
                            // Time expired ...
                            Ok(future::Either::B((_, more_cnc))) => {
                                cnc_task = more_cnc;
                                break;
                            }
                            Err(future::Either::A(((e, _), _)))
                            | Err(future::Either::B((e, _))) => return Err(e),
                        }
                    }
                }
                Meme::WaitForReconfigure(reasons) => {
                    warn!(log, "Waiting for a new configuration because: {}", reasons);
                    loop {
                        match core.run(cnc_task) {
                            Ok((Some(msg), more_cnc)) => match msg {
                                Message::Reconfigure(new_cfg) => {
                                    cnc_task = more_cnc.into_future();
                                    if *cfg.borrow() != new_cfg {
                                        info!(log, "New configuration!");
                                        cfg.replace(new_cfg);
                                        break;
                                    } else {
                                        info!(log, "Configuration reloaded but unchanged; still waiting...");
                                    }
                                }
                            },
                            Ok((None, _)) => {
                                return Err(Error::LostCnc);
                            }
                            Err((e, _)) => {
                                crit!(log, "unexpected error while reading cnc channel while waiting for new configuration");
                                return Err(e);
                            }
                        }
                    }
                }
            }
        }
    }
}

fn diff_membership<'a>(
    current: &'a Option<Vec<String>>,
    new: &'a Option<Vec<String>>,
) -> (Vec<&'a str>, Vec<&'a str>) {
    match (current, new) {
        (Some(current), Some(new)) => {
            let mut parts = Vec::with_capacity(current.len());
            let mut joins: BTreeSet<&str> = BTreeSet::from_iter(new.iter().map(|s| s.as_str()));
            for channel in current {
                if !joins.remove(channel.as_str()) {
                    // If a current channel isn't in the new channels we should part.
                    parts.push(channel);
                }
            }
            (
                parts.iter().map(|s| s.as_str()).collect(),
                joins.into_iter().collect::<Vec<&str>>(),
            )
        }
        _ => (
            current.iter().flatten().map(|s| s.as_str()).collect(),
            new.iter().flatten().map(|s| s.as_str()).collect(),
        ),
    }
}

fn expect_motd(message: &IrcMessage) -> Result<bool, Error> {
    match message.command {
        Command::NOTICE(..) => Err(Error::BadAuth), // error out ...
        Command::Response(rpl, ..) if rpl == Response::RPL_ENDOFMOTD => Ok(false),
        _ => Ok(true), // keep looking
    }
}

struct TrickledMessage<'a>(&'a RefCell<Config>, IrcMessage);

impl<'a> Trickle for TrickledMessage<'a> {
    /// how long to wait after sending
    fn moment_of_silence(&self) -> Duration {
        let rate_limits: &RateLimits = &self.0.borrow().rate_limits;
        match self.1.command {
            // TODO this should be determined by configuration ...
            Command::JOIN(_, _, _) => Duration::from_millis(rate_limits.join),
            Command::PART(_, _) => Duration::from_millis(rate_limits.part),
            _ => unimplemented!(),
        }
    }
}
