/// poor man's deserialization...
/// todo figure out how to do visitor patterns and maybe stop abusing macros
use chrono::prelude::NaiveDateTime;
use std::str::FromStr;
use uuid::Uuid;

use crate::twitch::event::TagError;

pub fn millis_to_timestamp(time: i64) -> Option<NaiveDateTime> {
    NaiveDateTime::from_timestamp_opt((time / 1_000) as i64, ((time % 1_000) * 1_000_000) as u32)
}

/// must be used in a function that returns Result<_, ::twitch::event::Error>
macro_rules! map_tags {
    ($tags:ident, $($key:tt => $var:ident: $typ:ty),+,) => {
        $(
        let mut $var: Option<&str> = None;
        )*
        for tag in $tags {
            let Tag(key, value) = tag;
            match key.as_ref() {
                $(
                $key => $var = value.as_ref().map(String::as_str),
                )*
                _ => return Err(TagError::Unexpected.for_key(key.as_str())),
            }
        }
        // TODO, we can make $typ optional since it can be inferred quite often ...
        $(
        let $var: $typ = $var.decode().map_err(|e| Error::Tag($key.into(), e))?;
        )*
    };
}

//pub type Result<T> = std::result::Result<T, TagError>;

pub trait Decoder<T>: Sized {
    fn decode(self) -> Result<T, TagError>;
}

/// optional things? when decoding into an Option<T>, if the source is None (no tag value was
/// found), then return that instead of an error
impl<'f, T> Decoder<Option<T>> for Option<&'f str>
where
    Option<&'f str>: Decoder<T>,
{
    fn decode(self) -> Result<Option<T>, TagError> {
        match self {
            None => Ok(None),
            Some(_) => self.decode().map(|t: T| Some(t)),
        }
    }
}

/// this is essentially the same as &str but maybe a little more clean when used to ignore tag values
impl<'f> Decoder<()> for Option<&'f str> {
    fn decode(self) -> Result<(), TagError> {
        self.map(|_| ()).ok_or(TagError::Missing)
    }
}

impl<'f> Decoder<&'f str> for Option<&'f str> {
    fn decode(self) -> Result<&'f str, TagError> {
        self.ok_or(TagError::Missing)
    }
}

impl<'f> Decoder<i32> for Option<&'f str> {
    fn decode(self) -> Result<i32, TagError> {
        self.decode()
            .and_then(|txt: &str| Ok(i32::from_str(txt.as_ref())?))
    }
}

impl<'f> Decoder<i64> for Option<&'f str> {
    fn decode(self) -> Result<i64, TagError> {
        self.decode()
            .and_then(|txt: &str| Ok(i64::from_str_radix(txt.as_ref(), 10)?))
    }
}

impl<'f> Decoder<chrono::NaiveDateTime> for Option<&'f str> {
    fn decode(self) -> Result<chrono::NaiveDateTime, TagError> {
        self.decode().and_then(|time| {
            millis_to_timestamp(time)
                .ok_or_else(|| TagError::Nope(format!("time out-of-range/invalid: {}", time)))
        })
    }
}

impl<'f> Decoder<Uuid> for Option<&'f str> {
    fn decode(self) -> Result<Uuid, TagError> {
        self.decode().and_then(|id| Ok(Uuid::parse_str(id)?))
    }
}

impl<'f> Decoder<bool> for Option<&'f str> {
    fn decode(self) -> Result<bool, TagError> {
        self.decode().and_then(|txt: &str| match txt {
            "0" => Ok(false),
            "1" => Ok(true),
            _ => Err(TagError::Bool(txt.into())),
        })
    }
}

/// for decoding into a serde_json::Value so it can be stored in postgres as bson, but this
/// probably should/could be modified to work any anything with the extend/from iterator trait?
#[derive(Debug)]
pub struct BadgeMap(serde_json::Map<String, serde_json::Value>);

impl<'f> Decoder<BadgeMap> for Option<&'f str> {
    fn decode(self) -> Result<BadgeMap, TagError> {
        Ok(BadgeMap(map_badges(self.decode()?)?))
    }
}

impl From<BadgeMap> for serde_json::Value {
    fn from(b: BadgeMap) -> Self {
        serde_json::Value::Object(b.0)
    }
}

fn map_badges(
    text: &str,
) -> std::result::Result<serde_json::Map<String, serde_json::Value>, TagError> {
    let mut map = serde_json::Map::new();
    if !text.is_empty() {
        for badge_version in text.split(',') {
            match badge_version.splitn(2, '/').collect::<Vec<_>>().as_slice() {
                &[badge, version] => map.insert(badge.to_owned(), version.parse::<i64>()?.into()),
                _ => {
                    return Err(TagError::Nope(format!("failed split {:?}", badge_version)));
                }
            };
        }
    }
    Ok(map)
}

#[cfg(test)]
mod test {
    use chrono::prelude::NaiveDateTime;
    use irc::proto::message::Tag;
    use irc::proto::Message;
    use std::str::FromStr;
    use uuid::Uuid;

    use super::{BadgeMap, Decoder};
    use crate::twitch::event::{Error, TagError};

    #[test]
    fn test_decode_uuid() {
        assert_eq!(
            "a0101429-9216-45b9-9c21-58e8a2cb0f67"
                .parse::<Uuid>()
                .unwrap(),
            Some("a0101429-9216-45b9-9c21-58e8a2cb0f67")
                .decode()
                .unwrap(),
        )
    }

    #[test]
    fn test_map_tags() -> Result<(), Error> {
        let msg = Message::from_str("@badges=moderator/1,subscriber/0;color=;display-name=moutonbot;emotes=;flags=;id=a0101429-9216-45b9-9c21-58e8a2cb0f67;mod=1;room-id=45686481;subscriber=1;tmi-sent-ts=1546242545862;turbo=0;user-id=279119976;user-type=mod :moutonbot!moutonbot@moutonbot.tmi.twitch.tv PRIVMSG #mrmouton :Youtube.com/MrMouton\r\n").unwrap();
        let tags = msg.tags.as_ref().unwrap();

        map_tags! {
            tags,
            "id" => id: Uuid,
            "display-name" => name: &str,
            "optional" => opt: Option<&str>,
            "subscriber" => is_sub: bool,
            "tmi-sent-ts" => ts: NaiveDateTime,
            // ignored but expected ...
            "badges" => _badges: (),
            "color" => _color: (),
            "emotes" => _emotes: (),
            "flags" => _flags: (),
            "mod" => _mod: (),
            "room-id" => _room_id: (),
            "turbo" => _turbo: (),
            "user-id" => _user_id: (),
            "user-type" => _user_type: (),
        };
        assert_eq!(id, "a0101429-9216-45b9-9c21-58e8a2cb0f67".parse().unwrap());
        assert_eq!(name, "moutonbot");
        assert_eq!(opt, None);
        assert_eq!(is_sub, true);
        assert_eq!(ts, "2018-12-31T07:49:05.862".parse().unwrap());
        Ok(())
    }

    #[test]
    fn test_badges() -> Result<(), Error> {
        let msg = Message::from_str(r"@badge-info=subscriber/12;badges=subscriber/12,premium/1 :tmi.twitch.tv USERNOTICE #potato").unwrap();
        let tags = msg.tags.as_ref().unwrap();
        map_tags! {
            tags,
            "badges" => badges: BadgeMap,
            "badge-info" => badge_info: BadgeMap,
        };
        assert_eq!(
            Into::<serde_json::Value>::into(badge_info),
            json!({"subscriber": 12})
        );
        assert_eq!(
            Into::<serde_json::Value>::into(badges),
            json!({"subscriber": 12, "premium": 1})
        );
        Ok(())
    }
}
