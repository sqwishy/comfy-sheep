/// reading twitch events (from irc messages)

#[macro_use]
mod tags;
pub mod event;
pub use self::event::{as_twitch_event, Event};
