/// event parsing (from irc messages)
use chrono::prelude::{DateTime, NaiveDateTime, Utc};
use irc::proto::message::Tag;
use irc::proto::response::Response;
use irc::proto::{Command, Message};
use uuid::Uuid;

// this is needed for the map_tags! macro
use super::tags;
use super::tags::Decoder;

pub type Result<T> = std::result::Result<T, Error>;

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Unimplemented
        NoTags
        NoSender
        NoChannel
        // Some generic container for an argument missing or having an invalid value. This could be
        // improved by adding more nuance, but the caller doesn't really care about all these
        // different values anyway.
        NoArgument(what: String) {
            display("missing/invalid irc command argument: {}", what)
        }
        Tag(key: String, err: TagError) {
            cause(err)
            display("regarding tag `{}`; {}", key, err)
        }
    }
}

quick_error! {
    #[derive(Debug)]
    pub enum TagError  {
        Missing { display("missing and expected") }
        Unexpected { display("unexpected tag in message") }
        NoValue
        Uuid(err: uuid::ParseError) {
            from()
        }
        Int(err: std::num::ParseIntError) {
            cause(err)
            from()
        }
        Bool(fuckywucky: String)
        Nope(wat: String) {
            description(wat)
        }
    }
}

impl TagError {
    #[inline]
    pub fn for_key<S>(self, key: S) -> Error
    where
        S: Into<String>,
    {
        Error::Tag(key.into(), self)
    }
}

/// remove irc channel name prefix (does nothing if the argument didn't start with one)
/// also only does this for #, not for & ! +
pub fn without_channel_prefix(name: &str) -> &str {
    if name.starts_with('#') {
        name.get(1..).unwrap_or_default()
    } else {
        name
    }
}

#[derive(Debug)]
pub enum Event<'a> {
    ChannelMessage {
        id: Uuid,
        ts: NaiveDateTime,
        // channel: &'a str,
        channel_id: i64,
        text: &'a str,
        // login: &'a str,
        user_id: i64,
        // display_name: &'a str,
        /// Comma-separated list of chat badges and the version of each badge (each in the format
        /// <badge>/<version>, such as admin/1). There are many valid badge values; e.g., admin,
        /// bits, broadcaster, global_mod, moderator, subscriber, staff, turbo. Many badges have
        /// only 1 version, but some badges have different versions (images), depending on how long
        /// you hold the badge status; e.g., subscriber.
        badges: serde_json::Value,
        badge_info: Option<serde_json::Value>,
        emotes: &'a str,
        bits: Option<i32>,
    },
    Host {
        timestamp: DateTime<Utc>,
        host: &'a str,
        target: &'a str,
        viewers: Option<i32>,
    },
    // Most of these are optional since we may receive all fields or just one.
    RoomState {
        timestamp: DateTime<Utc>,
        channel_id: i64,
        delay: Option<i32>,
        followers_only: Option<i32>,
        is_emote_only: Option<bool>,
        is_r9k: Option<bool>,
        is_subs_only: Option<bool>,
    },
    Subscription {
        id: Uuid,
        ts: NaiveDateTime,
        channel_id: i64,
        subscriber: i64,
        gifted_by: Option<i64>,
        months: i32,
        message: &'a str,
        plan: &'a str, // TODO this can be "Prime" ...
    },
    // Ritual {
    //     id: Uuid,
    //     ts: NaiveDateTime,
    // },
    // ClearChat {
    //     ts: NaiveDateTime,
    //     channel: i64,
    //     user: i64,
    //     ban_duration: Option<i32>,
    // },
    // ClearMessage {
    //     timestamp: DateTime<Utc>,
    //     target_message: Uuid,
    //     // channel: i64,
    // },
}

pub fn as_twitch_event(message: &Message, timestamp: DateTime<Utc>) -> Result<Option<Event>> {
    match message.command {
        // TODO channel might be us, right? should we just call it recipient?
        Command::PRIVMSG(_, ref text) => {
            let tags = message.tags.as_ref().ok_or(Error::NoTags)?;
            map_tags! { tags,
                "id" => id: Uuid,
                "display-name" => _display_name: (),
                "tmi-sent-ts" => ts: NaiveDateTime,
                "user-id" => user_id: i64,
                "bits" => bits: Option<i32>,
                "emotes" => emotes: &str,
                "room-id" => channel_id: i64,
                // wow type inference!
                "badges" => badges: tags::BadgeMap,
                "badge-info" => badge_info: Option<tags::BadgeMap>,
                // ignored & not type checked but expected ...
                "subscriber" => _is_sub: (),
                "color" => _color: (),
                "flags" => _flags: (),
                "mod" => _mod: (),
                "turbo" => _turbo: (),
                "user-type" => _user_type: (),
                // what is this even doing here?
                "emote-only" => _emote_only: Option<()>,
                // apparently this is something?
                "sent-ts" => _wat: Option<()>,
            };
            Ok(Event::ChannelMessage {
                id,
                ts,
                // login,
                user_id,
                // display_name,
                badges: serde_json::Value::from(badges),
                badge_info: badge_info.map(serde_json::Value::from),
                emotes,
                bits,
                channel_id,
                // channel: &channel,
                text: &text,
            }.into())
        }
        Command::NOTICE(_, ref msg) if msg == "Login authentication failed" => {
            Ok(None)
        }
        Command::NOTICE(_, ref msg) if msg.starts_with("Now hosting") => {
            // Captured by HOSTTARGET
            Ok(None)
        }
        Command::NOTICE(_, ref msg) if msg.starts_with("This room is") => {
            // Captured by ROOMSTATE
            Ok(None)
        }
        Command::JOIN(_, _, _) // TODO JOIN and PART should be payed attention to for tracking membership memes
        | Command::CAP(_, _, _, _)
        | Command::PING(_, _)
        | Command::PONG(_, _) => Ok(None),
        Command::Response(ref rpl, ref _args, ref _suffix) => match rpl {
            Response::RPL_MYINFO
            | Response::RPL_NAMREPLY
            | Response::RPL_ENDOFNAMES
            | Response::RPL_YOURHOST
            | Response::RPL_CREATED
            | Response::RPL_MOTDSTART
            | Response::RPL_MOTD
            | Response::RPL_ENDOFMOTD
            | Response::RPL_WELCOME => Ok(None),
            _ => Err(Error::Unimplemented),
        },
        Command::Raw(ref cmd, ref args, ref suffix) => {
             match cmd.as_ref() {
                 "USERNOTICE" => {
                     let tags = message.tags.as_ref().ok_or(Error::NoTags)?;
                     let message = suffix.as_ref().map(String::as_str).unwrap_or_default();
                     // We make two passes here, for msg-id values that we don't support, we don't
                     // want to have to explicitly ignore them. It's nicer to have specific match
                     // groups based on the value of the msg-id tag.
                     let msg_id: &str = tags.iter().find(|Tag(key, _)| key == "msg-id")
                         .ok_or(TagError::Missing)
                         .and_then(|Tag(_, value)| value.as_ref().ok_or(TagError::NoValue))
                         .map_err(|e| e.for_key("msg-id"))?;
                     // sub, resub, subgift, anonsubgift, submysterygift, giftpaidupgrade,
                     // rewardgift, anongiftpaidupgrade, raid, unraid, ritual, bitsbadgetier
                     match msg_id {
                         // Gift subscription ...
                         "subgift" | "anonsubgift" => {
                             map_tags! { tags,
                                 "id" => id: _,
                                 "tmi-sent-ts" => ts: _,
                                 "room-id" => channel_id: i64,
                                 "msg-id" => _msg_id: (),
                                 "user-id" => user_id: i64,
                                 "msg-param-months" => months: i32,
                                 "msg-param-sender-count" => _sender_count: Option<()>,
                                 "msg-param-mass-gift-count" => _gift_count: Option<()>,
                                 "msg-param-recipient-user-name" => _recipient_login: Option<()>,
                                 "msg-param-recipient-id" => recipient: i64,
                                 "msg-param-recipient-display-name" => _recipient_name: Option<()>,
                                 "msg-param-sub-plan-name" => _sub_plan_name: (),
                                 "msg-param-sub-plan" => plan: &str,
                                 // stuff we don't care about
                                 "login" => _login: Option<()>,
                                 "user-type" => _user_type: Option<()>,
                                 "subscriber" => _subscriber: Option<()>,
                                 "mod" => _mod: Option<()>,
                                 "badges" => _badges: Option<()>,
                                 "badge-info" => _badge_info: Option<()>,
                                 "color" => _color: Option<()>,
                                 "display-name" => _display_name: Option<()>,
                                 "emotes" => _emotes: Option<()>,
                                 "flags" => _flags: Option<()>,
                                 "system-msg" => _msg: Option<()>,
                                 // I don't know what his is or what it referrs to, the value looks like
                                 // checksum.
                                 "msg-param-origin-id" => _origin_id: Option<()>,
                                 // wtf??? Tag("msg-param-fun-string", Some("FunStringTwo"))
                                 "msg-param-fun-string" => _fun_string: Option<()>,
                             };
                             Ok(Event::Subscription {
                                 id,
                                 ts,
                                 channel_id,
                                 message,
                                 plan,
                                 gifted_by: Some(user_id),
                                 subscriber: recipient,
                                 months,
                             }.into())
                         },
                         // gift -> sub
                         "giftpaidupgrade" => {
                             map_tags! { tags,
                                 "id" => id: _,
                                 "tmi-sent-ts" => ts: _,
                                 "room-id" => channel_id: i64,
                                 "msg-id" => _msg_id: (),
                                 "user-id" => user_id: i64,
                                 // On a giftpaidupgrade this is the info for the original gifter
                                 // ... TODO this is actually interesting and I want to record it.
                                 "msg-param-sender-login" => _sender_login: Option<()>,
                                 "msg-param-sender-name" => _sender_name: Option<()>,
                                 // Stuff we don't care about.
                                 "login" => _login: Option<()>,
                                 "user-type" => _user_type: Option<()>,
                                 "subscriber" => _subscriber: Option<()>,
                                 "mod" => _mod: Option<()>,
                                 "badges" => _badges: Option<()>,
                                 "badge-info" => _badge_info: Option<()>,
                                 "color" => _color: Option<()>,
                                 "display-name" => _display_name: Option<()>,
                                 "emotes" => _emotes: Option<()>,
                                 "flags" => _flags: Option<()>,
                                 "system-msg" => _msg: Option<()>,
                                 "msg-param-fun-string" => _fun_string: Option<()>,
                             };
                             Ok(Event::Subscription {
                                 id,
                                 ts,
                                 channel_id,
                                 message,
                                 plan: "TODO",
                                 gifted_by: None,
                                 subscriber: user_id,
                                 // We don't have months from any tags?
                                 months: 2,
                             }.into())
                         }
                         // NOT gift subscription ...
                         "sub" | "resub" => {
                             map_tags! { tags,
                                 "id" => id: _,
                                 "tmi-sent-ts" => ts: _,
                                 "room-id" => channel_id: i64,
                                 "msg-id" => _msg_id: (),
                                 "user-id" => user_id: i64,
                                 // We still get this? But what are we to do with it if we get
                                 // cumulative-months?
                                 "msg-param-months" => _months: Option<()>,
                                 "msg-param-cumulative-months" => months: i32,
                                 "msg-param-sub-plan-name" => _sub_plan_name: (),
                                 "msg-param-sub-plan" => plan: &str,
                                 // Stuff we don't care about.
                                 "msg-param-should-share-streak" => _wat: Option<()>,
                                 "msg-param-streak-months" => _pls: Option<()>,
                                 "login" => _login: Option<()>,
                                 "user-type" => _user_type: Option<()>,
                                 "subscriber" => _subscriber: Option<()>,
                                 "mod" => _mod: Option<()>,
                                 "badges" => _badges: Option<()>,
                                 "badge-info" => _badge_info: Option<()>,
                                 "color" => _color: Option<()>,
                                 "display-name" => _display_name: Option<()>,
                                 "emotes" => _emotes: Option<()>,
                                 "flags" => _flags: Option<()>,
                                 "system-msg" => _msg: Option<()>,
                                 "msg-param-origin-id" => _origin_id: Option<()>,
                                 "msg-param-fun-string" => _fun_string: Option<()>,
                             };
                             Ok(Event::Subscription {
                                 id,
                                 ts,
                                 channel_id,
                                 message,
                                 plan,
                                 gifted_by: None,
                                 subscriber: user_id,
                                 months,
                             }.into())
                         },
                         _ => return Err(Error::Unimplemented),
                     }
                 }

                 "HOSTTARGET" => {
                     if message.tags.is_some() {
                         // We don't expect tags here, so if we get some, we want to know.
                         return Err(Error::Unimplemented);
                     }
                     let host = args.get(0).map(|s| without_channel_prefix(s)).ok_or(Error::NoChannel)?;
                     let suffix = suffix.as_ref().ok_or(Error::NoArgument("host target".into()))?;
                     let (target, count) = match suffix.splitn(2, ' ').collect::<Vec<_>>().as_slice() {
                         &[""] | &["-"] | &["", _] | &["-", _] => Err(Error::Unimplemented)?,
                         &[target] => (target, None),
                         &[target, ""] => (target, None),
                         &[target, "-"] => (target, None),
                         &[target, viewers] => (target, viewers.into()),
                         wat @ _ => return Err(Error::NoArgument(format!("splitn(2, ' ') can't return {:?}", wat))),
                     };
                     let viewers = if let Some(count) = count {
                         Some(count.parse().map_err(|e| Error::NoArgument(format!("host viewer count doesn't look numbery: {}", e)))?)
                     } else {
                         None
                     };
                     Ok(Event::Host { timestamp, host, target, viewers }.into())
                 }

                 "ROOMSTATE" => {
                     let tags = message.tags.as_ref().ok_or(Error::NoTags)?;
                     map_tags! { tags, 
                         "room-id" => channel_id: _,
                         "slow" => delay: _,
                         "followers-only" => followers_only: _,
                         "emote-only" => is_emote_only: _,
                         "r9k" => is_r9k: _,
                         "subs-only" => is_subs_only: _,
                         "rituals" => _rituals: Option<()>,
                     };
                     Ok(Event::RoomState {
                         timestamp,
                         channel_id,
                         delay,
                         followers_only,
                         is_emote_only,
                         is_r9k,
                         is_subs_only,
                     }.into())
                 }

                 // This doesn't work ... still not sure on the correct format. docs are wrong.
                 // "CLEARCHAT" => {
                 //     let tags = message.tags.as_ref().ok_or(Error::NoTags)?;
                 //     map_tags! { tags,
                 //         "tmi-sent-ts" => ts: _,
                 //         "ban-duration" => ban_duration: _,
                 //         "room-id" => channel: _,
                 //         "target-user-id" => user: _,
                 //     };
                 //     Ok(Event::ClearChat { ts, channel, user, ban_duration }.into())
                 // }
                 // "CLEARMSG" => {
                 //     let tags = message.tags.as_ref().ok_or(Error::NoTags)?;
                 //     map_tags! { tags,
                 //         "target-msg-id" => target_message: _,
                 //         // For some reason this is some stupid fucked up number like negative six
                 //         // gazillion, so we have to ignore it
                 //         "tmi-sent-ts" => _ts: _,
                 //         // this is the empty string? why does twitch hate nice things
                 //         "room-id" => _channel: Option<()>,
                 //         "login" => _login: Option<()>,
                 //     };
                 //     Ok(Event::ClearMessage { timestamp, target_message }.into())
                 // }

        //         // TODO it may be a good idea to handle this and use it for some internal status
        //         // thing? at least to treat as a successful auth instead of waiting for motd...
        //         "GLOBALUSERSTATE" => Ok(None),
        //         // We only seem to see this for ourselves, possibly because we don't care about the
        //         // membership capabilities...?
        //         "USERSTATE" => Ok(None),
                 _ => Err(Error::Unimplemented),
            }
        }
        _ => Err(Error::Unimplemented),
    }
}
