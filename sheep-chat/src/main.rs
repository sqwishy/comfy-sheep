extern crate chrono;
extern crate core;
extern crate futures;
extern crate irc;
extern crate itertools;
extern crate postgres;
extern crate rand;
extern crate signal_hook;

extern crate clap;

#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_journald;
extern crate slog_term;

extern crate tokio;
extern crate tokio_core;
extern crate toml;
extern crate uuid;

#[macro_use]
extern crate failure;

#[macro_use]
extern crate quick_error;

extern crate serde;
#[macro_use]
extern crate serde_derive;
#[cfg(test)]
#[macro_use]
extern crate serde_json;

extern crate comfy;

use std::boxed::Box;
use std::fs;
use std::io::Error as IoError;
use std::thread;

use slog::Drain;

use tokio::runtime::current_thread;

use futures::future;
use futures::sync::mpsc;
use futures::{Future, Sink, Stream};

use clap::Arg;

use signal_hook::iterator::Signals;

mod chat;
mod futile;
mod persist;
mod streamext;
mod twitch;

const DEFAULT_CONFIG_PATH: &str = "sheep-chat.toml";

#[derive(Debug, Fail)]
pub enum ReadConfigError {
    #[fail(display = "io")]
    Io(#[cause] IoError),
    #[fail(display = "toml format error")]
    Toml(#[cause] toml::de::Error),
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Config {
    pub irc: chat::Config,
    pub postgres: persist::Config,
}

#[derive(Debug)]
struct Chancellor {
    log: slog::Logger,
    cfg_path: String,
    cfg: Config,
    cnc: mpsc::Receiver<()>,
    chat_tx: mpsc::Sender<chat::Message>,
    pg_tx: mpsc::Sender<persist::Message>,
}

pub fn read_config(path: &str) -> Result<Config, ReadConfigError> {
    let cfg_bytes = fs::read(path).map_err(ReadConfigError::Io)?;
    Ok(toml::from_slice(&cfg_bytes).map_err(ReadConfigError::Toml)?)
}

impl Chancellor {
    pub fn run_forever(self) {
        let Chancellor {
            log,
            cfg_path,
            cfg,
            chat_tx,
            pg_tx,
            cnc,
        } = self;

        #[derive(Debug)]
        enum Wow {
            Signal(i32),
            Quit(),
        }

        let signals = Signals::new(&[
            signal_hook::SIGINT,
            signal_hook::SIGTERM,
            signal_hook::SIGUSR1,
        ])
        .expect("install signal handlers")
        .into_async()
        .expect("signal stream")
        .map(Wow::Signal)
        .map_err(|e: IoError| panic!(e));

        let cnc_stream = cnc
            .map(|_| unreachable!())
            .map_err(|e: ()| panic!(e))
            .chain(future::ok(Wow::Quit()).into_stream());

        type OhMyFuckingGod = Box<
            Future<
                Item = (
                    slog::Logger,
                    Config,
                    String,
                    mpsc::Sender<chat::Message>,
                    mpsc::Sender<persist::Message>,
                ),
                Error = (),
            >,
        >;

        let main = cnc_stream
            .select(signals)
            .fold(
                (log, cfg, cfg_path, chat_tx, pg_tx),
                |(log, cfg, cfg_path, chat_tx, pg_tx), wow| -> OhMyFuckingGod {
                    match wow {
                        Wow::Quit() | Wow::Signal(signal_hook::SIGINT) | Wow::Signal(signal_hook::SIGTERM) => {
                            // TODO don't necessarily error out here?
                            Box::new(future::err(()))
                        }
                        Wow::Signal(_) => {
                            info!(log, "Reloading configuration..."; "path" => cfg_path.clone());
                            match read_config(&cfg_path) {
                                Ok(new_cfg) => {
                                    let old = cfg;
                                    if old == new_cfg {
                                        info!(log, "Configuration unchanged.");
                                        Box::new(future::ok((log, old, cfg_path, chat_tx, pg_tx)))
                                    } else {
                                        info!(log, "New configuration loaded"; "configuration" => format!("{:#?}", new_cfg));
                                        Box::new(
                                            pg_tx
                                                .send(persist::Message::Reconfigure(new_cfg.postgres.clone()))
                                                .map_err(|e| panic!(e))
                                                .join(
                                                chat_tx
                                                    .send(chat::Message::Reconfigure(new_cfg.irc.clone()))
                                                    .map_err(|e| panic!(e)),
                                                )
                                                .map(|(pg_tx, chat_tx)| (log, new_cfg, cfg_path, chat_tx, pg_tx))
                                        )
                                    }
                                }
                                Err(e) => {
                                    error!(log, "Failed to read configuration file: {}", e);
                                    Box::new(future::ok((log, cfg, cfg_path, chat_tx, pg_tx)))
                                }
                            }
                        }
                    }
                },
            )
            .map(|_| ());

        current_thread::run(main);
    }
}

fn main() {
    let matches = clap::App::new("sheep-chat")
        .author("somebody@froghat.ca")
        .version(env!("CARGO_PKG_VERSION"))
        .args(&[
            Arg::from_usage("-c, --config <FILE> 'Path to config toml file'")
                .default_value(DEFAULT_CONFIG_PATH),
            Arg::from_usage("--journal 'Log to the journal'"),
        ])
        .get_matches();

    let log = {
        if matches.is_present("journal") {
            let drain = slog_journald::JournaldDrain.ignore_res();
            slog::Logger::root(drain, o!())
        } else {
            let decorator = slog_term::TermDecorator::new().build();
            let drain = slog_term::CompactFormat::new(decorator).build().fuse();
            let drain = slog_async::Async::new(drain).chan_size(2048).build().fuse();
            slog::Logger::root(drain, o!())
        }
    };

    main_chat_agent(log, matches);
}

fn main_chat_agent(log: slog::Logger, matches: clap::ArgMatches) {
    // TODO make path absolute?
    let cfg_path = matches.value_of("config").unwrap().to_string();
    let cfg = read_config(&cfg_path)
        .expect("valid configuration required at startup; sort yourself out, bucko");

    info!(log, "Initial configuration loaded"; "configuration" => format!("{:#?}", cfg));

    let (pg_tx, pg_rx) = mpsc::channel::<persist::Message>(16384);
    let (chat_tx, chat_rx) = mpsc::channel::<chat::Message>(8);
    let (chancellor_tx, chancellor_rx) = mpsc::channel::<()>(1);

    let chancellor = Chancellor {
        log: log.new(o!("fief" => "chancellor")),
        cfg_path,
        cfg: cfg.clone(),
        cnc: chancellor_rx,
        chat_tx,
        pg_tx: pg_tx.clone(),
    };

    let irc = chat::Fief {
        log: log.new(o!("fief" => "irc")),
        pg: pg_tx,
        cnc: chat_rx,
        cfg: cfg.irc,
    };

    let pg = persist::Fief {
        log: log.new(o!("fief" => "persist")),
        rx: pg_rx,
        cfg: cfg.postgres,
    };

    let chancellor_handle = {
        let log = log.clone();
        thread::Builder::new().name("chancellor".into()).spawn(|| {
            let log = log;
            chancellor.run_forever();
            info!(log, "chancellor shut down...");
        })
    }
    .unwrap();

    let pg_handle = {
        let log = log.clone();
        thread::Builder::new().name("pg".into()).spawn(|| {
            let log = log;
            // Move this the sender for the channel into here so that when this thread stops, the
            // sender is dropped and the channel closes, signaling to the chancellor to stop...
            let _chancellor_tx = chancellor_tx;
            pg.run_forever().expect("pg result"); // TODO
            info!(log, "pg shut down...");
        })
    }
    .unwrap();

    let irc_handle = {
        let log = log.clone();
        thread::Builder::new().name("irc".into()).spawn(|| {
            let log = log;
            irc.run_forever().expect("irc result"); // TODO
            info!(log, "irc shut down...");
        })
    }
    .unwrap();

    if let Err(e) = irc_handle.join() {
        info!(log, "irc thread fucked up with: {:?}", e)
    };

    if let Err(e) = pg_handle.join() {
        info!(log, "pg thread fucked up with: {:?}", e)
    };

    if let Err(e) = chancellor_handle.join() {
        info!(log, "chancellor thread fucked up with: {:?}", e)
    };

    info!(log, "We did it!");
}
