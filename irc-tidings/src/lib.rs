#[macro_use]
extern crate nom;

use std::convert::From;

// const TAGS_ERR: nom::ErrorKind = nom::ErrorKind::Custom(0u32);
// const PREFIX_ERR: nom::ErrorKind = nom::ErrorKind::Custom(1u32);
// const CMD_ERR: nom::ErrorKind = nom::ErrorKind::Custom(2u32);
// const PARAMS_ERR: nom::ErrorKind = nom::ErrorKind::Custom(3u32);

/// user = 1*( %x01-09 / %x0B-0C / %x0E-1F / %x21-3F / %x41-FF )
///          ; any octet except NUL, CR, LF, " " and "@"
/// (May not be utf-8?)
#[inline]
pub fn is_user_byte(chr: u8) -> bool {
    const NUL: u8 = 0x00;
    const LF: u8 = 0x0a;
    const CR: u8 = 0x0d;
    const SPACE: u8 = 0x20;
    const AT: u8 = 0x40;
    chr != NUL && chr != LF && chr != CR && chr != SPACE && chr != AT
}

/// special = %x5B-60 / %x7B-7D
///              ; "[", "]", "\", "`", "_", "^", "{", "|", "}"
/// (Should probably be utf-8?)
#[inline]
pub fn is_special(chr: u8) -> bool {
    (chr >= 0x5b && chr <= 0x60) || (chr >= 0x7b && chr <= 0x7d)
}

/// This is anything alphanumeric or "special" which effectively anything numeric or '-' or
/// between 0x41 and 0x7d inclusive...
/// nom::is_alphanumeric(chr) || is_special(chr) || chr == b'-'
/// (probably utf-8, right?)
#[inline]
pub fn is_nickname_byte(chr: u8) -> bool {
    nom::is_digit(chr) || (chr >= 0x41 && chr <= 0x7d) || chr == b'-'
}

#[inline]
named!(parse_nickname, take_while1!(is_nickname_byte));

#[inline]
named!(parse_user, take_while1!(is_user_byte));

/// This is gratuitous, because it seems that I get messages that don't conform to spec. and this
/// probably won't break anything
#[inline]
named!(
    parse_hostname,
    take_while1!(|chr| is_nickname_byte(chr) || chr == b'.')
);

#[inline]
named!(parse_host, tuple!(parse_hostname));

/// sequence of any characters except NUL, CR, LF, semicolon (`;`) and SPACE
#[inline]
pub fn is_escaped_value(chr: u8) -> bool {
    const NUL: u8 = 0x00;
    const LF: u8 = 0x0a;
    const CR: u8 = 0x0d;
    const SPACE: u8 = 0x20;
    const SEMICOLON: u8 = 0x3b;
    chr != NUL && chr != LF && chr != CR && chr != SPACE && chr != SEMICOLON
}

/// key is the entire part before the value defined as:
///   [ <vendor> '/' ] <sequence of letters, digits, hyphens (`-`)>
/// vendor, if Some, is a pair of slices to bytes before and after the '/'.
#[derive(Debug, PartialEq)]
pub struct Tag<'t> {
    key: &'t [u8],
    // vendor: Option<(&'t [u8], &'t [u8])>,
    escaped: Option<&'t [u8]>,
}

impl<'t> Tag<'t> {
    /// Character       Sequence in <escaped value>
    /// ; (semicolon)   \: (backslash and colon)
    /// SPACE           \s
    /// \               \\
    /// CR              \r
    /// LF              \n
    /// all others      the character itself
    pub fn unescaped(&self) -> String {
        String::new()
    }
}

// impl<'t> Tag<&'t [u8]> {
//     pub fn from_utf8(self) -> Result<Self, std::str::Utf8Error> {
//         let Tag(key, value) = self;
//         Ok(Tag(std::str::from_utf8(key)?, None))
//     }
// }

// couldn't figure out how to actually use this ...
// fn decorate_with_consumed<'f, F, O>(
//     f: &'f F,
// ) -> impl Fn(&'f [u8]) -> nom::IResult<&'f [u8], (&'f [u8], O)>
// where
//     F: Fn(&'f [u8]) -> nom::IResult<&'f [u8], O>,
// {
//     move |i: &[u8]| f(i).map(|(rest, out)| (rest, (&i[..i.len() - rest.len()], out)))
// }

// fn with_consumed<'f, F, O>(i: &'f [u8], f: &'f F) -> nom::IResult<&'f [u8], (&'f [u8], O)>
// where
//     F: Fn(&'f [u8]) -> nom::IResult<&'f [u8], O>,
// {
//     f(i).map(|(rest, out)| (rest, (&i[..i.len() - rest.len()], out)))
// }

// named!(
//     parse_tag_vendor_key<&[u8], (Option<&[u8]>, &[u8])>,
//     tuple!(
//         opt!(do_parse!(host: parse_host >> tag!(b"/") >> (host))),
//         take_while!(|chr| nom::is_alphanumeric(chr) || chr == b'-')
//     )
// );

/// This is quivalent to parse hostname (for the vendor part) and the / that delimits the vendor
/// and the rest of the key. parse_hostname is pretty flexible for some reason and includes
/// alphanumeric characters and hyphens, which should compose the rest of the tag key...
#[inline]
named!(
    parse_tag_key<&[u8], &[u8]>,
    take_while!(|chr| is_nickname_byte(chr) || chr == b'/' || chr == b'.')
);

#[inline]
named!(parse_tag<&[u8], Tag>,
    do_parse!(
        key: parse_tag_key >>
        escaped: opt!(tuple!(tag!(b"="), take_while!(is_escaped_value))) >>
        (Tag {
            key: key,
            escaped: escaped.map(|(_, e)| e)
        })
    )
);

#[inline]
named!(parse_tags<&[u8], Vec<Tag> >, separated_list!(tag!(b";"), parse_tag));

/// If we get just "foobar" I can't tell if that's a hostname or a nickname, so basically just
/// parse `name [ ! user ] [ @ host ]` and let the user figure it out...
#[derive(Debug, PartialEq)]
pub struct Prefix<'p> {
    name: &'p [u8],
    user: Option<&'p [u8]>,
    host: Option<&'p [u8]>,
}

impl<'p> Prefix<'p> {
    fn server(name: &'p [u8]) -> Self {
        Prefix {
            name,
            user: None,
            host: None,
        }
    }
}

#[inline]
named!(
    parse_prefix<&[u8], Prefix>,
    do_parse!(
        name: parse_host >>
        user: opt!(tuple!(tag!(b"!"), parse_user)) >>
        host: opt!(tuple!(tag!(b"@"), parse_host)) >>
        (Prefix {
            name, 
            user: user.map(|(_, user)| user),
            host: host.map(|(_, host)| host)
        })
    )
);

#[derive(Debug, PartialEq)]
pub enum Command<I> {
    Text(I),
    Numeric(u32),
}

#[inline]
named!(
    parse_command<&[u8], Command<&[u8]> >,
    alt!(
        take_while_m_n!(3, 3, nom::is_digit) => { |b| {
            let s = unsafe { std::str::from_utf8_unchecked(b) };
            Command::Numeric(s.parse().unwrap())
        }} |
        take_while1!(nom::is_alphabetic) => { |b| {
            //let s = unsafe { std::str::from_utf8_unchecked(b) };
            Command::Text(b)
        }}
    )
);

/// nospcrlfcl = %x01-09 / %x0B-0C / %x0E-1F / %x21-39 / %x3B-FF
///                ; any octet except NUL, CR, LF, " " and ":"
#[inline]
fn is_nospcrlfcl(chr: u8) -> bool {
    const NUL: u8 = 0x00;
    const LF: u8 = 0x0a;
    const CR: u8 = 0x0d;
    const SPACE: u8 = 0x20;
    const COLON: u8 = b':';
    chr != NUL && chr != LF && chr != CR && chr != SPACE && chr != COLON
}

/// trailing = *( ":" / " " / nospcrlfcl )
#[inline]
fn trailing(chr: u8) -> bool {
    const NUL: u8 = 0x00;
    const LF: u8 = 0x0a;
    const CR: u8 = 0x0d;
    chr != NUL && chr != LF && chr != CR
}

#[derive(Debug, PartialEq)]
pub struct Params<'p> {
    pub middle: Vec<&'p [u8]>,
    pub trailing: Option<&'p [u8]>,
}

impl<'p> From<Params<'p>> for Vec<&'p [u8]> {
    fn from(p: Params<'p>) -> Self {
        let Params {
            mut middle,
            trailing,
        } = p;
        if let Some(trailing) = trailing {
            middle.push(trailing);
        };
        middle
    }
}

impl<'p> From<Vec<&'p [u8]>> for Params<'p> {
    fn from(mut v: Vec<&'p [u8]>) -> Self {
        if let Some(tail) = v.pop() {
            Params {
                middle: v,
                trailing: Some(tail),
            }
        } else {
            Params {
                middle: v,
                trailing: None,
            }
        }
    }
}

#[inline]
named!(
    parse_params<&[u8], Params>,
    do_parse!(
        // TODO This is actually kind of expensive, mainly the vector ...
        middle: fold_many0!(
            tuple!(tag!(b" "), take_while1!(is_nospcrlfcl)),
            Vec::new(),
            |mut acc: Vec<_>, (_, param)| {acc.push(param); acc}
        ) >>
        trailing: opt!(tuple!(tag!(b" :"), take_while1!(trailing))) >>
        (Params{ middle, trailing: trailing.map(|(_, trailing)| trailing) })
    )
);

#[derive(Debug, PartialEq)]
pub struct Message<'m> {
    pub tags: Vec<Tag<'m>>,
    pub prefix: Option<Prefix<'m>>,
    pub command: Command<&'m [u8]>,
    pub params: Params<'m>,
}

#[inline]
named!(
    pub parse_message<&[u8], Message>,
    do_parse!(
        tags: opt!(tuple!(tag!(b"@"), parse_tags, tag!(b" ")))>>
        prefix: opt!(tuple!(tag!(b":"), parse_prefix, tag!(b" "))) >>
        command: parse_command >>
        params: parse_params >>
        tag!(b"\r\n") >>
        (Message{
            tags: tags.map(|(_, tags, _)| tags).unwrap_or(Vec::new()),
            prefix: prefix.map(|(_, prefix, _)| prefix),
            command,
            params: params,
        })
    )
);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tag() {
        let (_, tag) = parse_tag(b"badges=premium/1 ").unwrap();
        assert_eq!(
            tag,
            Tag {
                key: b"badges",
                escaped: Some(b"premium/1"),
            }
        );
    }

    #[test]
    fn vendor_tag() {
        let (_, tag) = parse_tag(b"example.com/ddd=eee ").unwrap();
        assert_eq!(
            tag,
            Tag {
                key: b"example.com/ddd",
                escaped: Some(b"eee"),
            },
        );
    }

    #[test]
    fn tags() {
        let (_, tags) = parse_tags(b"aaa;bbb= ").unwrap();
        assert_eq!(
            tags,
            vec![
                Tag {
                    key: b"aaa",
                    escaped: None,
                },
                Tag {
                    key: b"bbb",
                    escaped: Some(b""),
                },
            ]
        );
    }

    #[test]
    fn prefix_nick() {
        let (_, prefix) =
            parse_prefix(b"flusenwaffen!flusenwaffen@flusenwaffen.tmi.twitch.tv ").unwrap();
        assert_eq!(
            prefix,
            Prefix {
                name: b"flusenwaffen",
                user: Some(b"flusenwaffen"),
                host: Some(b"flusenwaffen.tmi.twitch.tv")
            }
        );
    }

    #[test]
    fn prefix_server() {
        let (_, prefix) = parse_prefix(b"tmi.twitch.tv ").unwrap();
        assert_eq!(prefix, Prefix::server(b"tmi.twitch.tv"));
    }

    #[test]
    fn prefix_with_underscores() {
        assert_eq!(
            parse_prefix(b"a_b!c_d@e_f.g.xyx "),
            Ok((
                &b" "[..],
                Prefix {
                    name: b"a_b",
                    user: Some(b"c_d"),
                    host: Some(b"e_f.g.xyx"),
                }
            ))
        );
    }

    #[test]
    fn params() {
        let (_, params) = parse_params(b" #destiny :Now hosting WhiteNervosa.\r\n").unwrap();
        assert_eq!(
            vec!["#destiny", "Now hosting WhiteNervosa."],
            Into::<Vec<_>>::into(params)
                .iter()
                .map(|p| std::str::from_utf8(p).unwrap())
                .collect::<Vec<_>>()
        );
    }
    #[test]
    fn command() {
        let (_, command) = parse_command(b"PING\r\n").unwrap();
        assert_eq!(command, Command::Text(&b"PING"[..]));
    }

    #[test]
    fn join_message() {
        let (_, msg) = parse_message(
            b":flusenwaffen!flusenwaffen@flusenwaffen.tmi.twitch.tv JOIN #mrmouton\r\n",
        )
        .unwrap();
        eprintln!("{:#?}", msg);
        assert_eq!(
            msg,
            Message {
                tags: vec![],
                prefix: Some(Prefix {
                    name: b"flusenwaffen",
                    user: Some(b"flusenwaffen"),
                    host: Some(b"flusenwaffen.tmi.twitch.tv")
                }),
                command: Command::Text(b"JOIN"),
                params: Params {
                    middle: vec![b"#mrmouton"],
                    trailing: None
                },
            }
        )
    }

    #[test]
    fn privmsg_message() {
        let (_, msg) =
            parse_message(b"@aaa=bbb :nick!ident@host.com PRIVMSG me :Hello\r\n").unwrap();
        eprintln!("{:#?}", msg);
        assert_eq!(
            msg,
            Message {
                tags: vec![Tag {
                    key: b"aaa",
                    escaped: Some(b"bbb"),
                }],
                prefix: Some(Prefix {
                    name: b"nick",
                    user: Some(b"ident"),
                    host: Some(b"host.com")
                }),
                command: Command::Text(b"PRIVMSG"),
                params: Params {
                    middle: vec![b"me"],
                    trailing: Some(b"Hello"),
                },
            }
        )
    }

    #[test]
    fn aaa_message() {
        let data = b"@aaaa-aaaa= :aaaaaa_aaaaaaaa!aaaaaa_aaaaaaaa@aaaaaa_aaaaaaaa.aaa.aaaaaa.aa AAAAAAA #aaaa_aaa :@Aaaaaaaa_AaAAa aa aaaaa aaaaaaa\r\n";
        assert_eq!(
            parse_message(data),
            Ok((
                &b""[..],
                Message {
                    tags: vec![Tag {
                        key: b"aaaa-aaaa",
                        escaped: Some(b""),
                    }],
                    prefix: Some(Prefix {
                        name: b"aaaaaa_aaaaaaaa",
                        user: Some(b"aaaaaa_aaaaaaaa"),
                        host: Some(b"aaaaaa_aaaaaaaa.aaa.aaaaaa.aa"),
                    }),
                    command: Command::Text(b"AAAAAAA"),
                    params: Params {
                        middle: vec![b"#aaaa_aaa"],
                        trailing: Some(b"@Aaaaaaaa_AaAAa aa aaaaa aaaaaaa"),
                    }
                }
            ))
        );
    }

    #[test]
    fn clearchat_message() {
        let data = b"@ban-duration=600;room-id=65171890;target-user-id=187107099;tmi-sent-ts=1550337850782 :tmi.twitch.tv CLEARCHAT #rainbow6 :derpglitcher\r\n";
        assert_eq!(
            parse_message(data),
            Ok((
                &b""[..],
                Message {
                    tags: vec![
                        Tag {
                            key: b"ban-duration",
                            escaped: Some(b"600"),
                        },
                        Tag {
                            key: b"room-id",
                            escaped: Some(b"65171890"),
                        },
                        Tag {
                            key: b"target-user-id",
                            escaped: Some(b"187107099"),
                        },
                        Tag {
                            key: b"tmi-sent-ts",
                            escaped: Some(b"1550337850782"),
                        },
                    ],
                    prefix: Some(Prefix::server(b"tmi.twitch.tv")),
                    command: Command::Text(b"CLEARCHAT"),
                    params: Params {
                        middle: vec![b"#rainbow6"],
                        trailing: Some(b"derpglitcher"),
                    },
                }
            ))
        );
    }

    /// reads a byte stream of irc messages
    /// ex
    /// $ psql my_db -qAtR '' -c 'select message from my_irc_logs' | head -n -1 > test_data
    #[test]
    fn wow() {
        let meme = include_bytes!("../test_data");
        let mut data = &meme[..];
        while data.len() > 0 {
            match parse_message(data) {
                Ok((more, _msg)) => {
                    //eprintln!("{:?}", _msg);
                    data = more;
                }
                Err(e) => {
                    let msg_str = if let Some(msg_end) = data.windows(2).position(|i| i == b"\r\n")
                    {
                        std::str::from_utf8(&data[..msg_end + 2])
                    } else {
                        std::str::from_utf8(&data)
                    }
                    .expect("message is not utf-8?");
                    println!("Failed with {} reading {:?}", e, msg_str);
                    panic!(e)
                }
            }
        }
    }
}
