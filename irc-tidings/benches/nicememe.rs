#[macro_use]
extern crate criterion;
extern crate irc;

use std::str::FromStr;

use criterion::Criterion;

use irc_tidings::parse_message;

const TEST_DATA: &'static [u8] = include_bytes!("../test_data_small");

fn iter_lines(bytes: &'static [u8]) -> impl Iterator<Item = String> {
    use std::io::{self, BufRead};
    let mut cursor = io::Cursor::new(bytes);
    std::iter::repeat_with(move || {
        let mut buf = String::new();
        if 0 == cursor.read_line(&mut buf).unwrap() {
            None
        } else {
            Some(buf)
        }
    })
    .take_while(Option::is_some)
    .flatten()
}

//fn parse_messages() -> () {
//    let mut remaining = &TEST_DATA[..];
//    while remaining.len() > 0 {
//        let (more, _) = parse_message(remaining).unwrap();
//        remaining = more;
//    }
//}

fn criterion_benchmark(c: &mut Criterion) {
    let msg_bufs = iter_lines(TEST_DATA).collect::<Vec<String>>();
    let num_msgs = msg_bufs.len();

    use criterion::{Benchmark, Throughput};

    c.bench(
        "nice meme",
        Benchmark::new("irc-tidings", move |b| {
            b.iter(|| {
                msg_bufs
                    .iter()
                    .for_each(|buf| parse_message(&buf.as_bytes()).map(|_| ()).unwrap())
            })
        })
        //.with_function("irc", move |b| {
        //    b.iter(|| {
        //        msg_bufs.iter().for_each(|buf| {
        //            irc::proto::message::Message::from_str(buf)
        //                .map(|_| ())
        //                .unwrap()
        //        })
        //    })
        //})
        .throughput(Throughput::Elements(num_msgs as u32)),
    );
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
