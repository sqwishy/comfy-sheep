incomplete IRC parsing library

It seems to work (not crash or outright fail to parse) on all the messages I tried from
some twitch logs a while ago. But the rust API doesn't really exist and nothing is
decoded from bytes into strings.

Also it allocates a vector somewhere and I think maybe instead of that it could return
an iterator, and then I think it wouldn't make any allocations...
