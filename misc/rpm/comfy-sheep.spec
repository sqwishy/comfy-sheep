Name:           comfy-sheep
Version:        0.0.9
Release:        2%{?dist}
Summary:        Comfy Sheep
URL:            https://gitlab.com/sqwishy/comfy-sheep

License:        TODO
Source0:        %{name}-%{version}.tar.gz

Provides: user(comfy-sheep)
Provides: group(comfy-sheep)

%global __python %{__python3}

%if 0%{?is_opensuse}
BuildRequires: coreutils python3 python3-pip python3-devel python3-setuptools python3-wheel
Requires(pre): shadow
Requires: python3 shadow
%else
BuildRequires: coreutils python3 python3-pip python3-devel python3-setuptools python3-wheel
Requires(pre): shadow-utils
Requires: python3 shadow-utils
%endif

%description

%prep
# pls halp

%pre
getent group comfy-sheep > /dev/null  || groupadd comfy-sheep --system
getent passwd comfy-sheep > /dev/null || \
    useradd comfy-sheep --system -g comfy-sheep \
        --comment "Comfy Sheep Service" \
        --home-dir /etc/comfy-sheep \
        --create-home \
        --skel /dev/null

%install
# I don't know what the fuck I'm doing
mkdir -p %{buildroot}
tar xvzf %{SOURCE0} -C %{buildroot}
%{__python} -m pip install -v comfy \
    --no-build-isolation \
    --no-index \
    --find-links %{buildroot}/%{_prefix}/lib/comfy-sheep/py-src \
    --target     %{buildroot}/%{_prefix}/lib/comfy-sheep/site-packages
rm -rf %{buildroot}/%{_prefix}/lib/comfy-sheep/py-src

%files

%{_prefix}/bin/sheep-scrape
%{_prefix}/bin/sheep-chat
%{_prefix}/bin/sheep-backfill
%{_prefix}/bin/sheep-vacuum
%{_prefix}/bin/tsdb-latest-chunks

%{_prefix}/lib/systemd/system/comfy-sheep.target
%{_prefix}/lib/systemd/system/sheep-scrape.service
%{_prefix}/lib/systemd/system/sheep-scrape.timer
%{_prefix}/lib/systemd/system/sheep-chat@.service
%{_prefix}/lib/systemd/system/sheep-vacuum.service
%{_prefix}/lib/systemd/system/sheep-balance.service

%{_prefix}/lib/comfy-sheep/site-packages

%{_prefix}/share/comfy-sheep/sql
%{_prefix}/share/comfy-sheep/README.rst

%attr(770,comfy-sheep,comfy-sheep) %{_sysconfdir}/comfy-sheep
%config(noreplace) %attr(600,comfy-sheep,comfy-sheep) %{_sysconfdir}/comfy-sheep/env
%config(noreplace) %attr(600,comfy-sheep,comfy-sheep) %{_sysconfdir}/comfy-sheep/sheep-chat-default.toml

%license
%doc


%changelog

