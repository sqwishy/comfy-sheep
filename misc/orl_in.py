"""parses subscriptions from OverRustleLogs and inserts into postgres.
"""

import argparse
import asyncio
import datetime
import logging
import re

import attr

import asyncpg
import uvloop

logger = logging.getLogger("shep-in")


async def main(dbstring, channel, infile):
    con = await asyncpg.connect(dbstring)
    stmt = await con.prepare(
        """
        insert into p.chat_subs (id, time, channel, subscriber, tier, resub, gifted_by, message)
            values (null, $1, $2, $3, $4, $5, $6, $7)
        """
    )
    async with con.transaction():
        for sub in parse_subs(infile, channel):
            await stmt.fetch(*attr.astuple(sub))


TIME_PAT = re.compile(
    r"^\[(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})"
    r" (?P<hour>\d{1,2}):(?P<minute>\d{1,2}):(?P<second>\d{1,2}) UTC\]"
)
SUB_PAT = re.compile(
    r"(?P<subscriber>\w+) just subscribed.*(?P<tier>(?:Tier \d+ sub)|(?:Twitch Prime))"
)
RECIDIVATE_PAT = re.compile(r"(?P<subscriber>\w+) is continuing the Gift Sub they got")
GIFT_PAT = re.compile(
    r"(?P<gifted_by>\w+) gifted.*(?P<tier>(?:Tier \d+ sub)|(?:Twitch Prime)).*to (?P<subscriber>\w+)"
)
DURATION_PAT = re.compile(r"subscribed for (\d+) months in a row")
MESSAGE_PAT = re.compile(r"\[SubMessage\]: (?P<message>.*)")


def parse_subs(infile, channel):
    for line in infile:
        if line:
            m = TIME_PAT.search(line)
            if m is None:
                logger.error("Failed to find time in: %s", line)
                continue
            _, pos = m.span()

            time = datetime.datetime(
                **{k: int(v) for k, v in m.groupdict().items()},
                tzinfo=datetime.timezone.utc,
            )

            sub_match = SUB_PAT.search(line, pos=pos) or GIFT_PAT.search(line, pos=pos)
            if sub_match is None:
                logger.error("Failed to find sub in: %s", line)
                continue
            _, pos = sub_match.span()

            resub_match = DURATION_PAT.search(line, pos=pos)
            if resub_match is not None:
                resub = int(resub_match.group(1)) - 1
                _, pos = resub_match.span()
            else:
                resub = 0

            m = MESSAGE_PAT.search(line, pos=pos)
            if m is None:
                message = ""
            else:
                message = m.group("message")

            yield Subscription(
                time=time,
                channel=channel,
                **sub_match.groupdict(),
                message=message,
                resub=resub,
            )


@attr.s
class Subscription(object):
    time = attr.ib()
    channel = attr.ib()
    subscriber = attr.ib()
    # Yikes ...
    tier = attr.ib(
        converter=lambda s: 0 if s == "Twitch Prime" else int(s.split(" ")[1])
    )
    resub = attr.ib(default=0)
    gifted_by = attr.ib(default="")
    message = attr.ib(default="")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--infile", default="-", type=argparse.FileType("r"))
    parser.add_argument("--db", default="postgres:///shep")
    parser.add_argument("--channel")
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)

    logger.debug("args: %s", args)

    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()

    loop.run_until_complete(main(args.db, args.channel, args.infile))
