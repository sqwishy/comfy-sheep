#[macro_use]
extern crate slog;

extern crate clap;

use rand::Rng;
use std::borrow::Borrow;
use std::iter::{repeat, Iterator};
use std::time;
use std::time::Duration;

mod iter;
mod log;
mod stopwatch;

pub use iter::hungry_iter;
pub use log::{new_logger, Logable};
pub use stopwatch::Stopwatch;

pub fn duration_to_f64<D: Borrow<time::Duration>>(d: D) -> f64 {
    const NANOS_PER_SEC: u32 = 1_000_000_000;
    (d.borrow().as_secs() as f64) + (d.borrow().subsec_nanos() as f64) / (NANOS_PER_SEC as f64)
}

const MIN_DELAY_EXP: u32 = 5;
const MAX_DELAY_EXP: u32 = 14;

pub fn new_delays() -> impl Iterator<Item = Duration> {
    let mut rng = rand::thread_rng();
    (MIN_DELAY_EXP..MAX_DELAY_EXP)
        .into_iter()
        .chain(repeat(MAX_DELAY_EXP))
        .map(move |exp| (2u64.pow(exp) as f64 * rng.gen_range(0.7, 1.3)) as u64)
        .map(Duration::from_millis)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
