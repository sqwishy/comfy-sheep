use std::cell::RefCell;
use std::collections::VecDeque;
use std::iter::Iterator;
use std::rc::Rc;

pub struct Iter<T>(Rc<RefCell<VecDeque<T>>>);
pub type Sink<T> = Rc<RefCell<VecDeque<T>>>;

impl<T> Iterator for Iter<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        let Iter(refcell) = self;
        refcell.borrow_mut().pop_front()
    }
}

pub fn hungry_iter<T>(deque: VecDeque<T>) -> (Sink<T>, Iter<T>) {
    let rc = Rc::new(RefCell::new(deque));
    (rc.clone(), Iter(rc))
}

mod test {
    use super::*;
    use std::collections::VecDeque;

    #[test]
    fn test() {
        let (sink, source) = hungry_iter(VecDeque::new());

        let mut scanner = source.scan(0, |sum, n| {
            *sum += n;
            Some(*sum)
        });

        assert_eq!(scanner.next(), None);

        sink.borrow_mut().extend(2..4);
        assert_eq!(scanner.next(), Some(2));
        assert_eq!(scanner.next(), Some(5));
        assert_eq!(scanner.next(), None);

        sink.borrow_mut().extend(2..4);
        assert_eq!(scanner.next(), Some(7));
        assert_eq!(scanner.next(), Some(10));
        assert_eq!(scanner.next(), None);
    }
}
