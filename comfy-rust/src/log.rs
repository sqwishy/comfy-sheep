use std::time;

use slog::Drain;

pub enum Logable {
    Duration(time::Duration),
}

impl From<time::Duration> for Logable {
    fn from(d: time::Duration) -> Self {
        Logable::Duration(d)
    }
}

impl slog::Value for Logable {
    fn serialize(
        &self,
        _rec: &slog::Record,
        key: slog::Key,
        serializer: &mut slog::Serializer,
    ) -> slog::Result {
        match self {
            Logable::Duration(duration) => {
                serializer.emit_f64(key, crate::duration_to_f64(duration))
            }
        }
    }
}

pub fn new_logger(matches: &clap::ArgMatches) -> slog::Logger {
    if matches.is_present("journal") {
        let drain = slog_journald::JournaldDrain.ignore_res();
        slog::Logger::root(drain, o!())
    } else {
        let decorator = slog_term::TermDecorator::new().build();
        let drain = slog_term::CompactFormat::new(decorator).build().fuse();
        let drain = slog_async::Async::new(drain).chan_size(2048).build().fuse();
        slog::Logger::root(drain, o!())
    }
}
