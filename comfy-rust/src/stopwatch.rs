use std::time;

pub struct Stopwatch(time::Instant);

impl Stopwatch {
    pub fn start() -> Self {
        Stopwatch(time::Instant::now())
    }

    pub fn reset(self) -> (Self, time::Duration) {
        let since = self.since();
        (Self::start(), since)
    }

    pub fn since(&self) -> time::Duration {
        time::Instant::now() - self.0
    }
}
